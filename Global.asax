﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup
        RegisterRoutes(RouteTable.Routes);
    }

    void RegisterRoutes(RouteCollection routes)
    {
        routes.MapPageRoute("Home", "", "~/Pages/Home.aspx", true);
        routes.MapPageRoute("AboutUs", "company/about-us", "~/Pages/About_Us.aspx", true);
        routes.MapPageRoute("OurTeam", "company/our-team", "~/Pages/our-team.aspx", true);
        routes.MapPageRoute("Career", "company/careers", "~/Pages/careers.aspx", true);
        routes.MapPageRoute("B2BService", "product/b2b-service", "~/Pages/B2BService.aspx", true);
        routes.MapPageRoute("B2CServices", "product/b2c-service", "~/Pages/B2CServices.aspx", true);
        routes.MapPageRoute("AccountingService", "product/accounting-software", "~/Pages/Accounting-Software.aspx", true);
        routes.MapPageRoute("FlightModule", "product/flight-booking-module", "~/Pages/Flight-Booking-Module.aspx", true);
        routes.MapPageRoute("HotelBookingService", "product/hotel-booking-system", "~/Pages/Hotel-Booking-System.aspx", true);
        routes.MapPageRoute("HowWeWork", "company/how-we-work", "~/Pages/how-we-work.aspx", true);
        routes.MapPageRoute("missionvision", "company/mission-vision", "~/Pages/mission-vision.aspx", true);
        routes.MapPageRoute("OurValues", "company/our-values", "~/Pages/our-values.aspx", true);
        routes.MapPageRoute("PackageModule", "product/package-module", "~/Pages/Package-Module.aspx", true);
        routes.MapPageRoute("TravelManagement", "product/travel-management", "~/Pages/Travel-Management.aspx", true);
        routes.MapPageRoute("TravelCRM", "product/travel-crm", "~/Pages/Travel(CRM).aspx", true);
        routes.MapPageRoute("whitelabel", "product/white-label", "~/Pages/white-label.aspx", true);
        routes.MapPageRoute("XMLAPI", "product/xml-api", "~/Pages/XML-API.aspx", true);
        routes.MapPageRoute("Contact", "company/contact-us", "~/Pages/Contact-Us.aspx", true);
       
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

</script>

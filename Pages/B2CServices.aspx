﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Mater.master" AutoEventWireup="true" CodeFile="B2CServices.aspx.cs" Inherits="Pages_B2CServices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .hr {
            border-top: 2px solid #e61651;
            font-weight: 700;
            width: 50px;
            margin-bottom: 10px;
        }

    </style>

    <section class="header-inner bg-overlay-black-50" style="background-image: url('../images/header-inner/17.jpg');">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="header-inner-title text-center position-relative">
                        <h1 class="text-white fw-normal">Service Detail</h1>
                        <p class="text-white mb-0">The best way is to develop and follow a plan. Start with your goals in mind and then work backwards to develop the plan.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="space-ptb">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <div class="service-details">
                        <h5 class="fw-bold">B2C Travel Portal – Complete Online Travel Booking Portal</h5>
                        <div class="hr"></div>
                        <p>B2C Travel Portal is online travel booking portal which provides online search and book functionality with flight, hotel, tour, transfer and package module to enable global B2C consumers to search and book online.</p>
                        <p>B2C Travel Portal is key component for a Travel Website. It is travel portal for retail sales enables business to sell more products and increase revenue.</p>


                        <h5 class="fw-bold">Which is B2C Travel Portal Development Company who offers best B2C Travel Portal/ Travel Booking Engine?</h5>
                        <div class="hr"></div>
                        <p>Amlin Technology is best B2C Travel Portal Development Company providing B2C Travel Portal and Online Travel Booking Portal to build a complete robust Travel Booking System for Tour Operator, Travel Agency and Travel Company to maximize revenues and spot top position among travel industry competitors.</p>


                        <h5 class="fw-bold">What is Online Travel Booking Portal and How it is Beneficial?</h5>
                        <div class="hr"></div>
                        <p>Online Travel Booking Portal is the complete travel booking software solution for travel agency and tour operator. Through this Online travel booking portal, customer can search and book their travel service (hotels, flight, transfer, packages, activities) online and pay online easily.</p>
                        <p>Amlin Technology is one of the leading travel technology company  develop Online travel booking portal  for travel agents and tour operator to increase their booking with brand awareness and  complete their business goal.</p>

                        <h5 class="fw-bold">Why Choose Amlin Technology for your Online Travel Booking Portal?</h5>
                        <div class="hr"></div>
                        <p>Amlin Technology offers the best online travel booking portal for travel agent and tour operator. Hotels, Flights, Tours, Transfers, Train, Car Rental all in one Packages. 100+ xml suppliers, flight GDS, customized packages, dynamic packages all are in online travel booking portal.</p>
                        <p>Our professional developer develops the online travel booking portal  with best travel technology solution based on client requirements.</p>

                        <ul class="list list-unstyled mt-3">
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>B2C is an online travel booking engine which works across the global market and as the name suggest B2C is directly done between business and consumer. B2C booking engine platform is an user friendly interface where customers can also book as guest users in it.</span>
                            </li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Eventually, it helps the owner for developing their business online to acquire more booking of hotels, thus gaining more profit which in turn enhances your business in travel industry. Likewise, in B2C booking engine it has the direct connection between business to consumer who are the ultimate consumers of brands and services.</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>B2C Booking Engine offers Booking Specials, Tours Package, End moment offers, Easy and simple reservations for hotels and safest payments options for B2C users.</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>This magnificent booking engine platform lets you grab B2C customers to make more sales. Excellent checklist, instinctive UIs and finest of its category bring the edge.</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Totally mobile ready. Your travel website will glance and execute uniformly well on Work Stations, mobile phones with tablets no extra coding or design work.</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>B2C reservation engine is developed with exclusive technology to convey the highest quality application. Server, bandwidth and performance are the stuff that we take care.</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Analyze and Boost intuition into your online travel booking engine's growth. See which parts are selling more and twist your marketing drive accordingly to enhance and promote the travel products to end-users.</span></li>
                        </ul>


                        <h5 class="fw-bold">We have best Online travel booking portal solution:</h5>
                        <div class="hr"></div>

                        <ul class="list list-unstyled mt-3">
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Hotels, Flights, Tours, Transfers, Train, Car Rental all in one package</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>100+ xml hotel supplier worldwide</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Flight GDS connectivity</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Customized Packages</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Accounting Payable and Receivable Control</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Email and Social Media Flyer Generator</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Multi-Level Markup Management</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Payment Gateways Integration</span></li>

                        </ul>

                        <br />

                        <h5 class="fw-bold">Benefits:</h5>
                        <div class="hr"></div>

                        <ul class="list list-unstyled mt-3">
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Increase visibility and reach of your business across global markets.</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Attend customers during non-working hours also thus you can’t miss any enquiry.</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Automated processes reduce human efforts required for attend customer request.</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Attract more customers by showcasing and highlight product information such as photos, features, facilities, reviews, rating and contact information.</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Build Customer base and loyalty by selling products directly to customer at best price.</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Sell multiple travel products under one brand</span></li>


                        </ul>


                        <br />

                        <h5 class="fw-bold">Features:</h5>
                        <div class="hr"></div>

                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list list-unstyled mt-3">
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Multi-currency support</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Multilingual support</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Shopping cart facility</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Banner Management</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Barcode scanner API</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Interactive portal for end user</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Static package & Dynamic package</span></li>

                                </ul>
                            </div>

                            <div class="col-md-6">
                                <ul class="list list-unstyled mt-3">
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Customers can book as guest user</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Desired Payment gateway integration</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Mobile responsive website</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>User friendly interface</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Google maps & street view</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Weather tools</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Auto alerts on confirmation, bookings, tickets</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Comprehensive MIS reports</span></li>


                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>


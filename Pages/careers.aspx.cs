﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.Net.Mime;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

public partial class Pages_careers : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString.ToString());

    protected void Page_Load(object sender, EventArgs e)
    {
       // lbs.Visible = false;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        lbs.Visible = true;

        try
        {
            string path = "";

            if (chooseFile.HasFile)
            {
                chooseFile.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + "/UploadDoc/" + chooseFile.FileName);
                path = chooseFile.FileName;
            }

            con.Open();
            SqlCommand oCommand = new SqlCommand("InsertCandidateDetails", con);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.Add("@full_name", SqlDbType.VarChar).Value = Convert.ToString(txt_fullname.Text);
            oCommand.Parameters.Add("@mobile", SqlDbType.VarChar).Value = Convert.ToString(txt_mob_no.Text);
            oCommand.Parameters.Add("@email", SqlDbType.VarChar).Value = Convert.ToString(txt_email1.Text);
            oCommand.Parameters.Add("@resume", SqlDbType.VarChar).Value = path;
            oCommand.ExecuteNonQuery();
            con.Close();
            SendApplicantEmail(txt_fullname.Text.ToString(), txt_email1.Text.ToString());
            SendToAdminEmail(txt_fullname.Text.ToString(), txt_mob_no.Text.ToString(), txt_email1.Text.ToString(),path);
            txt_fullname.Text = "";
            txt_mob_no.Text = "";
            txt_email1.Text = "";
            Response.Redirect("~");
        }
        catch (Exception exx)
        {
            con.Close();
            lbs.Visible = false;
            Response.Redirect("~");


        }

    }
    protected void SendApplicantEmail(string fullname, string ToEmail)
    {
        string mailto = "";
        String HtmlBody = "";
        string mailfrom = "";
        string smtp = "";
        string userid = "";
        string pass = "";
        string SubmitDate = "";

        try
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM EmaiL_Temp WHERE USER_Name='ToApplicant'", con);

            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                HtmlBody = dr["Html_Body"].ToString();
               

            }
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }

       
        try
        {
            SqlCommand cmd2 = new SqlCommand("SELECT * FROM Email_Crendentials WHERE username='admin'", con);
            cmd2.CommandType = CommandType.Text;
            //SqlCommand cmd2 = new SqlCommand("SELECT * FROM Email_Crendentials where username='" + user1 + "'", con);

            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            SqlDataReader dr2 = cmd2.ExecuteReader();
            while (dr2.Read())
            {

                mailfrom = dr2["mailfrom"].ToString();
                smtp = dr2["smtpclient"].ToString();
                userid = dr2["userid"].ToString();
                pass = dr2["password"].ToString();
                mailto = dr2["email"].ToString();
                HtmlBody = dr2["Html_Body"].ToString();

            }
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }
        string subject = "Thank You for submitting your details.";
        DateTime now = DateTime.Now;
        SubmitDate = now.ToString("dd/MM/yyyy");
        HtmlBody = HtmlBody.Replace("#date", SubmitDate).Replace("#Applicant", fullname);
        login ls = new login();
        int i = ls.SendMail(ToEmail, mailfrom, smtp, userid, pass, HtmlBody, subject,"");
        if (i > 0)
        {
            lbs.Text = "Submitted Successfuly";
           Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LoggedInScript", "alert('Thank you for submitting, we will contact you very soon')", true);
            lbs.Visible = false;
        }
        else
        {
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LoggedInScript", "alert('Wotk Assign Successfully, Mail not sent to develpoer ')", true);
            lbs.Visible = false;

        }
    }
    protected void SendToAdminEmail(string fullname, string mobile,string email,string path)
    {
        String HtmlBody = "";
        string mailto = "";
        string mailfrom = "";
        string smtp = "";
        string userid = "";
        string pass = "";
        string SubmitDate = "";
        try
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM EmaiL_Temp WHERE USER_Name='ToAdmin'", con);

            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                HtmlBody = dr["Html_Body"].ToString();


            }
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }
        try
        {
            SqlCommand cmd2 = new SqlCommand("SELECT * FROM Email_Crendentials WHERE username='admin'", con);
            cmd2.CommandType = CommandType.Text;
            //SqlCommand cmd2 = new SqlCommand("SELECT * FROM Email_Crendentials where username='" + user1 + "'", con);

            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            SqlDataReader dr2 = cmd2.ExecuteReader();
            while (dr2.Read())
            {

                mailfrom = dr2["mailfrom"].ToString();
                smtp = dr2["smtpclient"].ToString();
                userid = dr2["userid"].ToString();
                pass = dr2["password"].ToString();
                mailto = dr2["email"].ToString();
                HtmlBody = dr2["Html_Body"].ToString();
            }
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }




        string subject = "New Applicant " + " " + fullname;
        DateTime now = DateTime.Now;
        SubmitDate = now.ToString("dd/MM/yyyy");
        HtmlBody = HtmlBody.Replace("#Name", fullname).Replace("#Mobile", mobile).Replace("#email", email);
        login ls = new login();
        int i = SendMail1(mailfrom, mailfrom, smtp, userid, pass, HtmlBody, subject, path);
        if (i > 0)
        {
            lbs.Visible = false;

        }
        else
        {
           // Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LoggedInScript", "alert('Wotk Assign Successfully, Mail not sent to develpoer ')", true);
            lbs.Visible =false;

        }
    }
    private MemoryStream PDFGenerate(string message, string ImagePath)
    {
        MemoryStream output = new MemoryStream();
        Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, output);
        pdfDoc.Open();
        Paragraph Text = new Paragraph(message);
        pdfDoc.Add(Text);
        byte[] file;
        file = System.IO.File.ReadAllBytes(ImagePath);
        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(file);
        jpg.ScaleToFit(550F, 200F);
        pdfDoc.Add(jpg);
        pdfWriter.CloseStream = false;
        pdfDoc.Close();
        output.Position = 0;
        return output;
    }
    public int SendMail1(string toEMail, string from,  string smtpClient, string userID, string pass, string body, string subject, string path)
    {
        System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
        System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
        msgMail.To.Clear();
        msgMail.To.Add(new System.Net.Mail.MailAddress(toEMail));
        msgMail.From = new System.Net.Mail.MailAddress(from);
        //string docpath = "/UploadDoc/" + path;

        //MemoryStream file = new MemoryStream(PDFGenerate("This is pdf file text", Server.MapPath(docpath)).ToArray());

        //file.Seek(0, SeekOrigin.Begin);
        //Attachment data = new Attachment(file, "RunTime_Attachment.pdf", "application/pdf");
        //ContentDisposition disposition = data.ContentDisposition;
        //disposition.CreationDate = System.DateTime.Now;
        //disposition.ModificationDate = System.DateTime.Now;
        //disposition.DispositionType = DispositionTypeNames.Attachment;

        if (chooseFile.HasFile)
        {
            string FileName = Path.GetFileName(chooseFile.PostedFile.FileName);
            msgMail.Attachments.Add(new Attachment(chooseFile.PostedFile.InputStream, FileName));
        }
        //  msgMail.Attachments.Add(new System.Net.Mail.Attachment(AttachmentFile));


        msgMail.Subject = subject;
        msgMail.IsBodyHtml = true;
        msgMail.Body = body;


        try
        {
            objMail.Credentials = new System.Net.NetworkCredential(userID, pass);
            objMail.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            objMail.EnableSsl = false;

            objMail.Port = 25;
            objMail.Host = smtpClient;
            // objMail.Timeout = 10000
            objMail.Send(msgMail);
            return 1;
        }
        catch (Exception ex)
        {
            // clsErrorLog.LogInfo(ex);
            return 0;
        }
    }

}
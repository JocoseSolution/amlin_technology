﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Mater.master" AutoEventWireup="true" CodeFile="B2BService.aspx.cs" Inherits="Pages_B2BService" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .counter {
            background: white;
            padding: 10px;
            border-radius: 5px;
            -webkit-box-shadow: 5px 5px 24px 0px rgb(2 45 98 / 10%);
            box-shadow: 5px 5px 24px 0px rgb(2 45 98 / 10%);
        }

          .hr {
            border-top: 2px solid #e61651;
            font-weight: 700;
            width: 50px;
            margin-bottom: 10px;
        }
            .counter .counter-icon i {
                color: #499fd5;
                margin-right: 25px;
                font-size: 43px;
                line-height: 70px;
            }
    </style>


    <section class="header-inner bg-overlay-black-50" style="background-image: url('../images/header-inner/17.jpg');">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="header-inner-title text-center position-relative">
                        <h1 class="text-white fw-normal">B2B Travel Portal</h1>
                        <p class="text-white mb-0">The best way is to develop and follow a plan. Start with your goals in mind and then work backwards to develop the plan.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="space-ptb">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <div class="service-details">
                        <h4 class="fw-bold">Best B2B Travel Portal</h4>
                        <p class="">B2B Travel Portal is online booking engine and important components for every travel agent and provides white label solution to get access a real time online bookings and availability to travel agents and tour operators with best user friendly booking engine platform.</p>
                        <p class="">In demands of the B2B Travel Portal, Travel Agency having access to third party supplier’s inventories which want best B2B Travel Portal or B2B Travel Booking Engine because using this B2B Booking Engine travel agent sold their travel product around the globe through the sub agent network and increase revenue.</p>
                        <p class="">Being a leading B2B Travel Portal provider, Amlin Technology should be the first choice for global travel agent.</p>



                        <div class="row justify-content-center">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <a href="#" class="bg-light p-4 text-center border-radius mb-4 d-block">
                                    <img class="img-fluid w-25" src="../images/svg/icon/001-refresh.svg" alt="">
                                    <h5 class="mt-4">Start a new business and get idea</h5>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <a href="#" class="bg-light p-4 text-center border-radius mb-4 d-block">
                                    <img class="img-fluid w-25" src="../images/svg/icon/060-pencil.svg" alt="">
                                    <h5 class="mt-4">Start Business based on Portal (B2B/B2C)</h5>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <a href="#" class="bg-light p-4 text-center border-radius mb-4 d-block">
                                    <img class="img-fluid w-25" src="../images/svg/icon/093-letter.svg" alt="">
                                    <h5 class="mt-4">Connect Global Supplier Inventory</h5>
                                </a>
                            </div>

                        </div>


                        <h5 class="fw-bold">Which is the Best B2B Travel Portal Development Company?</h5>
                        <div class="hr"></div>

                        <p>Amlin Technology is the best B2B Travel Portal Development provide best B2B Travel Portal with third party API Integration of flight booking and hotel booking for travel agent. Our Experienced travel professionals develop Advanced features B2B Booking Engine such as Hotel, Tour, Package, and Visa that enables you to upload your own contracted products, integrate third-party suppliers, adjust pricing and markup at runtime, generate agent wise reports, map banners, incorporate business rules and easily take insight into business sales.</p>
                        <p>With wide range of B2B Travel Portal features, Authorized agents can logon to your B2B portal, search and book products, add mark-up and pay for booking online. From Admin booking panel, they can view, email, or download booking voucher or cancel booking of their customer from any device at any time.</p>
                        <p>Robust and scalable platform and complemented by advanced B2B Travel Portal our developer provide the best B2B Travel Portal of hotels, flight and other travel service to travel agent , tour operators in their existing system and their own brand to connect more customer across the globe. We also provide XML API Integration for travel module to get real time booking services and became best B2B Travel Portal Provider.</p>


                        <h5 class="fw-bold">What are benefits of B2B module?</h5>
                        <div class="hr"></div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 mb-4">

                                <div class="counter">
                                    <div class="counter-icon">
                                        <i class="icofont-wall-clock"></i>
                                    </div>
                                    <div class="counter-content">
                                        <span style="font-size: 20px; font-weight: 600; color: #636363;">Real Time Booking Management</span>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 mb-4">

                                <div class="counter">
                                    <div class="counter-icon">
                                        <i class="icofont-smart-phone"></i>
                                    </div>
                                    <div class="counter-content">
                                        <span style="font-size: 20px; font-weight: 600; color: #636363;">Mobile Friendly Design</span>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 mb-4">

                                <div class="counter">
                                    <div class="counter-icon">
                                        <i class="icofont-ui-calendar"></i>
                                    </div>
                                    <div class="counter-content">
                                        <span style="font-size: 20px; font-weight: 600; color: #636363;">Automated Booking Confirmation</span>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 mb-4">

                                <div class="counter">
                                    <div class="counter-icon">
                                        <i class="icofont-dashboard-web"></i>
                                    </div>
                                    <div class="counter-content">
                                        <span style="font-size: 20px; font-weight: 600; color: #636363;">Fully Customization System</span>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 mb-4">

                                <div class="counter">
                                    <div class="counter-icon">
                                        <i class="icofont-money-bag"></i>
                                    </div>
                                    <div class="counter-content">
                                        <span style="font-size: 20px; font-weight: 600; color: #636363;">Saved time and money</span>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 mb-4">

                                <div class="counter">
                                    <div class="counter-icon">
                                        <i class="icofont-credit-card"></i>
                                    </div>
                                    <div class="counter-content">
                                        <span style="font-size: 20px; font-weight: 600; color: #636363;">Multiple Payment Integration</span>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 mb-4">

                                <div class="counter">
                                    <div class="counter-icon">
                                        <i class="icofont-dollar"></i>
                                    </div>
                                    <div class="counter-content">
                                        <span style="font-size: 20px; font-weight: 600; color: #636363;">Multicurrency support</span>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 mb-4">

                                <div class="counter">
                                    <div class="counter-icon">
                                        <i class="icofont-question-circle"></i>
                                    </div>
                                    <div class="counter-content">
                                        <span style="font-size: 20px; font-weight: 600; color: #636363;">Multilanguage support</span>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 mb-4">

                                <div class="counter">
                                    <div class="counter-icon">
                                        <i class="icofont-air-ticket"></i>
                                    </div>
                                    <div class="counter-content">
                                        <span style="font-size: 20px; font-weight: 600; color: #636363;">Easily set up Markup</span>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 mb-4">

                                <div class="counter">
                                    <div class="counter-icon">
                                        <i class="icofont-earth"></i>
                                    </div>
                                    <div class="counter-content">
                                        <span style="font-size: 20px; font-weight: 600; color: #636363;">Largest Network in Global environment</span>

                                    </div>
                                </div>
                            </div>




                        </div>




                        <h5 class="fw-bold">Which is the best features of B2B Travel Portal And Why Travel Agent uses B2B Travel Portal?</h5>
                        <div class="hr"></div>

                        <p class="">B2B Travel Portal Features</p>

                        <div class="row">
                            <ul class="list list-unstyled col-md-6 col-xs-12">
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Agent registration/login</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Define agent’s type – Cash or Credit</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Create sub agent(s)</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Set profile of agents</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Build own package</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Agent wise mark up</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Flexible markup & commission settings</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Check Booking</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Cancel Booking</span></li>

                            </ul>

                            <ul class="list list-unstyled col-md-6 col-xs-12">
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Issue online voucher</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Invoice & receipts</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Payment Gateway Integration</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Auto alerts on confirmation, bookings, tickets</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Comprehensive MIS reports</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Mobile responsive Design</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>User friendly interface</span></li>
                                <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Real Time Reporting</span></li>


                            </ul>
                        </div>




                    <br />
                        <br />
                     
                        <h5 class="fw-bold mb-4">Please contact us via below from for more info</h5>
                        <div class="hr"></div>
                        <form class="row">
                            <div class="col-md-4 mb-4">
                                <input type="text" class="form-control" id="exampleInputName" placeholder="Name">
                            </div>
                            <div class="col-md-4 mb-4">
                                <input type="text" class="form-control" id="exampleInputLname" placeholder="Last Name">
                            </div>
                            <div class="col-md-4 mb-4">
                                <input type="email" class="form-control" id="exampleInputEmail" placeholder="Email Address">
                            </div>
                            <div class="col-md-12 mb-4">
                                <textarea class="form-control" rows="5" id="exampleInputEnquiry" placeholder="Enquiry Description"></textarea>
                            </div>
                            <div class="col-md-12 mb-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                    <label class="form-check-label" for="flexCheckDefault">
                                        i agree to talk about my project with Hi-soft
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12 mb-0">
                                <button type="submit" class="btn btn-primary">Send Massage<i class="fas fa-arrow-right ps-3"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Mater.master" AutoEventWireup="true" CodeFile="Travel(CRM).aspx.cs" Inherits="Pages_Travel_CRM_" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <style type="text/css">
        .hr {
            border-top: 2px solid #e61651;
            font-weight: 700;
            width: 50px;
            margin-bottom: 10px;
        }

        p {
            text-align: justify;
        }
         </style>



    <section class="header-inner bg-overlay-black-50" style="background-image: url('../images/header-inner/17.jpg');">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="header-inner-title text-center position-relative">
                        <h1 class="text-white fw-normal">Travel CRM</h1>
                      
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    

    <section class="space-ptb">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <div class="service-details">

                       
                        <h5 class="fw-bold">What is Travel CRM?</h5>
                        <div class="hr"></div>
                        <p>Travel CRM is Customer Relationship Management Software that helps travel companies to manage leads, streamline follow ups, respond queries and have quality conversations with customers to develop strong customer relationship to improve sales.</p>
                        <p>Travel CRM, short for Customer Relationship Management, is travel software exclusively designed for travel agents, tour operators, travel companies and destination management companies to automate the entire operations such as managing and assigning leads, responding to queries, streamlining follow-ups, automated email alerts and advance reporting to improve sales efficiency and maximize revenues.</p>
                        <p>This Travel CRM Software allows travel agents and travel companies to manage leads, improve customer interaction, and automate business process to improve customer relationship and increase sales and retention.</p>
                        <p>In competitive industry, travel agents want to get travel software that manage each and every aspect of project, simplify workflow and improve business efficiency.</p>
                        <p>There are many Travel CRM Software available but with different features, so it is important to choose the right software that is exact match for your business.</p>
                        <p>In this time, Amlin Technology is a leading Travel Software Development Company provides the best Travel CRM Software for travel companies to automate day to day operations such as lead management, responding to queries, follow-ups, automated email to increase business leads and sales efficiency.</p>
                      
                         <img class="img-fluid border-radius mb-4 mb-md-5" src="../images/bg/travelcrm.jpg" />


                        <h5 class="fw-bold">Our Latest Project</h5>
                        <div class="hr"></div>

                        <div class="row">
                            <div class="col-md-6">
                        <p>Things fell in place for Rayna Tours & Travels (RTT) – one of the UAE’s premier travel service providers, upon the purchase of Amlin Technology’s feature-packed task management system. In the past, they used several software systems, which though worked to an extent to sort out the tasks and responsibilities, didn’t encompass all the elements to effectively organize and coordinate the jobs, manpower and tools in a more effective and consistent manner. But with the deployment of Amlin Technology’s TMS - featuring unique filtering tool, customizable email notification, easy to understand interface, and an integrated time monitoring system, RTT has been able to efficiently:</p>
                       </div>

                            <div class="col-md-6">
                                <img class="img-fluid border-radius mb-4 mb-md-5" src="../images/bg/Travel-CRM.jpg" />
                            </div>
                            
                            </div>

                        <ul class="list list-unstyled mt-3">
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Track key milestones</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Set task goals</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Manage workload</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Adapt to new project assignments with much ease</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Boost the productivity of the workforce</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Minimize stress & save time</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Complete major tasks on-time</span></li>
                           
                            


                        </ul>

                        <br />
                        <br />

                        <hr />



                        <div class="row">
                            <div class="col-md-6">
                                <h5 class="fw-bold">What set our Travel CRM System apart?</h5>

                                    <ul class="list list-unstyled mt-3">

                                        







                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Efficient task management features</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Configurable application</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Filtering feature that help to organize key tasks</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Folders to group tasks</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Easy to understand interface</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Integrated time monitoring system</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Customizable email notification</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Efficient reporting system</span></li>
                       
                            


                        </ul>
                            </div>
                            <div class="col-md-6">
                                <h5 class="fw-bold">Benefits and Solutions</h5>

                                    <ul class="list list-unstyled mt-3">
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Uninterrupted flow of business processes</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Eliminate the need to remember all key tasks</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Effectively set dependencies</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Improve the efficiency and productivity of employees</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Significantly cut down expenses</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Ensure top quality of project</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>On-time completion of project</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Track significant milestones</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Task reporting made easy</span></li>
                          
                            










                        </ul>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>


</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Mater.master" AutoEventWireup="true" CodeFile="XML-API.aspx.cs" Inherits="Pages_XML_API" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .hr {
            border-top: 2px solid #e61651;
            font-weight: 700;
            width: 50px;
            margin-bottom: 10px;
        }

        p {
            text-align: justify;
        }

        .feature-info-content {
            background: #027bc5;
            text-align: left;
            padding: 10px;
            width: 100%;
            margin: 0px;
            -webkit-box-shadow: 5px 5px 24px 0px rgb(2 45 98 / 10%);
            box-shadow: 5px 5px 24px 0px rgb(2 45 98 / 10%);
            border-radius: 5px;
        }

        .feature-info-style-08 .feature-info-item {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            padding-bottom: 30px;
            position: relative;
        }

        .feature-info-content {
            background: #ffffff;
            text-align: left;
            padding: 10px;
            width: 100%;
            margin: 0px;
            -webkit-box-shadow: 5px 5px 24px 0px rgb(2 45 98 / 10%);
            box-shadow: 5px 5px 24px 0px rgb(2 45 98 / 10%);
            border-radius: 5px;
            border: 1px solid #eee;
        }
    </style>



    <section class="header-inner bg-overlay-black-50" style="background-image: url('https://www.jpmorgan.com/content/dam/jpm/merchant-services-europe/insights/payments-optimisation/banner-how-an-api-first-approach.jpg');">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="header-inner-title text-center position-relative">
                        <h1 class="text-white fw-normal">XML-API Integration</h1>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="space-ptb">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <div class="service-details">
                        <p>XML API Integration is ultimately a source of connection which acts as interpreter between customer and supplier permitting numerous formats or programing languages. By this method the detailed information about hotel provider, car rental provider and many more can easily displayed. Travel XML API Integration provides real-time booking platform and integrated booking software. This helps you streamline opportunities and cater to multiple guest requirements in the most efficient manner.</p>

                        <p>Embedding XML API Integration into your travel business website can produce supple and modernize form, without doing bigger investment in costly and sophisticated design plans.</p>


                        <img class="img-fluid border-radius mb-4 mb-md-5" src="https://www.darinaholidays.ae/img/xmlapi-img.png" alt="">


                        <p>Amlin, a leading a Travel Portal Development Company, expertize in implementing third party API XML integration and XML integration for global travel companies , in travel trade we provide end to end API integration solution and build B2B / B2C travel booking engine through the XML connectivity of many hotel provider company like Within-Earth, GTA, Hotelbeds, RTS, RoomsXML etc.</p>

                        <h5 class="fw-bold">Our Services</h5>
                        <div class="hr"></div>

                        <div class="col-sm-12 col-lg-12 mb-12 mb-lg-0">
                            <div class="feature-info">
                                <div class="feature-info-content">
                                    <h5 class="mb-3 fw-normal feature-info-title">Hotel API Integrations</h5>
                                    <p>With Amlin Technology’s XML API, B2B and B2C websites can create a powerful online booking system. After all, it comes with options that help for integration of multiple hotel XMLs including DOTW, Hotelbeds and Hotelspro, thus providing your visitors with access to global hotel inventories.</p>

                                </div>
                            </div>
                        </div>

                        <br />
                        <div class="col-sm-12 col-lg-12 mb-12 mb-lg-0">
                            <div class="feature-info">
                                <div class="feature-info-content">
                                    <h5 class="mb-3 fw-normal feature-info-title">Tour API Integrations</h5>
                                    <p>Utilizing Our XML API, tour and travel companies – let it be B2B or B2C – can connect their web pages to our’s systems via Rayna Tours, GTA Transfers, and GTA Tours. While it allows B2B websites as well as their clients to view reports & check booking, cancellation and invoicing details.</p>

                                </div>
                            </div>
                        </div>
                        <br />

                        <div class="col-sm-12 col-lg-12 mb-12 mb-lg-0">
                            <div class="feature-info">
                                <div class="feature-info-content">
                                    <h5 class="mb-3 fw-normal feature-info-title">Payment Gateway Integration</h5>
                                    <p>Amlin's XML API enables to integrate Payment Getaway application with clients’ websites and online portals to manage payments and process credit cards in a simple yet safe manner.</p>

                                </div>
                            </div>
                        </div>

                       








                        <br />
                        <br />
                        <h5 class="fw-bold">Our XML API Integration has following features:</h5>
                        <div class="hr"></div>


                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list list-unstyled mt-3">
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Easy Connectivity</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>24x7 Bookings Worldwide</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>B2B and B2C Online Reservation Engine</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Integrated Backoffice system</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Payment Gateway</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Seamless integration with third party applications</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>No pricey investment needed</span></li>

                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Access to remote database</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Access to client information</span></li>


                                </ul>
                            </div>

                            <div class="col-md-6">
                                <ul class="list list-unstyled mt-3">
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Easy retrieval of product info with description & pricing</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Convenient tracking of order info, billing, & shipping details</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Eliminate unwanted expenses</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Easy to amend and update content</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>User-friendly interface</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Security protection</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Less chance for human errors</span></li>
                                    <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Enhanced sales and productivity</span></li>




                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>


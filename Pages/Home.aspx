﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Mater.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section class="banner slider-02">
        <div class="swiper-container swiper-container-initialized swiper-container-horizontal">
            <div class="swiper-wrapper h-700 h-sm-500" style="transition-duration: 0ms; transform: translate3d(-2526px, 0px, 0px);">
                <div class="swiper-slide align-items-center d-flex responsive-overlap-md bg-overlay-black-30 swiper-slide-duplicate swiper-slide-duplicate-active" style="background-image: url(../images/slider/04.jpg); background-size: cover; background-position: center center; width: 1263px;" data-swiper-slide-index="1">
                    <div class="swipeinner container">
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-10 text-center">
                                <h1 data-swiper-animation="fadeInUp" data-duration="1s" data-delay="0.25s" style="visibility: hidden;"><span class="text-stroke-white ">We Are Digital</span> <span class="font-large">Creative Agency</span></h1>
                                <h6 data-swiper-animation="fadeInUp" data-duration="1s" data-delay="0.5s" style="visibility: hidden;">Award-Winning website design &amp; creative digital agency</h6>
                                <a class="btn btn-dark btn-round text-white" data-swiper-animation="fadeInUp" data-duration="1s" data-delay="0.75s" href="#" style="visibility: hidden;">Read More<i class="fas fa-arrow-right ps-3"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide align-items-center d-flex responsive-overlap-md bg-overlay-black-30 swiper-slide-prev swiper-slide-duplicate-next" style="background-image: url(../images/slider/03.jpg); background-size: cover; background-position: center center; width: 1263px;" data-swiper-slide-index="0">
                    <div class="swipeinner container">
                        <div class="row justify-content-center">
                            <div class="col-xl-7 col-md-9 text-center">
                                <h1 data-swiper-animation="fadeInUp" data-duration="1s" data-delay="0.25s" class="" style="visibility: visible; animation-duration: 1s; animation-delay: 0.25s;"><span class="font-large">B2B Booking Engine</span></h1>
                                <h6 data-swiper-animation="fadeInUp" data-duration="1s" data-delay="0.5s" class="" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s;">B2B Solutions for travel and destination management companies to distribute their products and services across the globe via sub-agent network</h6>
                                <a class="btn btn-dark btn-round text-white" data-swiper-animation="fadeInUp" data-duration="1s" data-delay="0.75s" href="#" data-bs-toggle="modal" data-bs-target="#exampleModal" style="visibility: visible; animation-duration: 1s; animation-delay: 0.75s;">Get Started Now<i class="fas fa-arrow-right ps-3"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide align-items-center d-flex responsive-overlap-md bg-overlay-black-30 swiper-slide-active" style="background-image: url(../images/slider/04.jpg); background-size: cover; background-position: center center; width: 1263px;" data-swiper-slide-index="1">
                    <div class="swipeinner container">
                        <div class="row justify-content-center">
                            <div class="col-xl-8 col-lg-10 text-center">
                                <h1 data-swiper-animation="fadeInUp" data-duration="1s" data-delay="0.25s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.25s;" class="fadeInUp animated"><span class="text-stroke-white ">We Are Digital</span> <span class="font-large">Creative Agency</span></h1>
                                <h6 data-swiper-animation="fadeInUp" data-duration="1s" data-delay="0.5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s;" class="fadeInUp animated">Award-Winning website design &amp; creative digital agency</h6>
                                <a class="btn btn-dark btn-round text-white fadeInUp animated" data-swiper-animation="fadeInUp" data-duration="1s" data-delay="0.75s" href="#" style="visibility: visible; animation-duration: 1s; animation-delay: 0.75s;">Read More<i class="fas fa-arrow-right ps-3"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide align-items-center d-flex responsive-overlap-md bg-overlay-black-30 swiper-slide-duplicate swiper-slide-next swiper-slide-duplicate-prev" style="background-image: url(../images/slider/03.jpg); background-size: cover; background-position: center center; width: 1263px;" data-swiper-slide-index="0">
                    <div class="swipeinner container">
                        <div class="row justify-content-center">
                            <div class="col-xl-7 col-md-9 text-center">
                                <h1 data-swiper-animation="fadeInUp" data-duration="1s" data-delay="0.25s" style="visibility: hidden;"><span class="font-large">B2B Booking Engine</span></h1>
                                <h6 data-swiper-animation="fadeInUp" data-duration="1s" data-delay="0.5s" style="visibility: hidden;">A totally new way to grow your business</h6>
                                <a class="btn btn-dark btn-round text-white" data-swiper-animation="fadeInUp" data-duration="1s" data-delay="0.75s" href="#" style="visibility: hidden;">Get Started Now<i class="fas fa-arrow-right ps-3"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-button-prev" tabindex="0" role="button" aria-label="Previous slide"><i class="fas fa-arrow-left icon-btn"></i></div>
            <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide"><i class="fas fa-arrow-right icon-btn"></i></div>
            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
        </div>
    </section>


    <section class="space-pt">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 mb-4">
            <div class="border border-radius p-5 p-md-4 h-100">
              <span class="display-4 text-primary mb-3 d-block">50+</span>
              <h4 class="">Successfully completed projects</h4>
            </div>
          </div>
          <div class="col-lg-5 col-md-6 mb-4">
            <div class="h-100 p-5 p-md-3 border-radius d-flex text-center align-items-end bg-overlay-black-10" style="background-image:url(https://images.pexels.com/photos/380769/pexels-photo-380769.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940);    background-position: center;background-size: contain;">
              <span class="text-stroke-white fw-bold font-xlll position-relative">Working at Amlin</span>
            <div id="jarallax-container-0" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden; pointer-events: none; z-index: -100;"><div style="background-position: 50% 50%; background-size: cover; background-repeat: no-repeat; background-image: url(https://img.youtube.com/vi/9No-FiEInLA/maxresdefault.jpg); position: fixed; top: 0px; left: 358.667px; width: 451px; height: 272.1px; overflow: hidden; pointer-events: none; margin-top: 168.45px; transform: translate3d(0px, 364.95px, 0px); display: none;"></div></div></div>
          </div>
          <div class="col-lg-4 col-md-6 mb-4">
            <div class="bg-primary border-radius p-5 p-md-4 h-100">
              <div class="d-flex align-items-center">
                <span class="pe-4 pt-1 display-4 text-white">20+</span>
                <h5 class="mb-0 text-white">Qualified employees</h5>
              </div>
              <p class="text-white mt-3">Work with our leaders and the hard-working personalities who deliver innovative concepts to corporations like yours.</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
            <div class="h-100 bg-overlay-black-60 bg-holder border-radius p-4 pt-5" style="background-image: url(../images/gallery/07.jpg);">
              <div class="position-relative text-center pt-4">
                <ul class="list-unstyled list-inline text-white pt-4">
                  <li class="list-inline-item me-1 font-xl text-warning"><i class="fas fa-star"></i></li>
                  <li class="list-inline-item me-1 font-xl text-warning"><i class="fas fa-star"></i></li>
                  <li class="list-inline-item me-1 font-xl text-warning"><i class="fas fa-star"></i></li>
                  <li class="list-inline-item me-1 font-xl text-warning"><i class="fas fa-star"></i></li>
                  <li class="list-inline-item me-1 font-xl text-warning"><i class="fas fa-star-half"></i></li>
                </ul>
                <h5 class="mb-0 text-white">Rated 4.80 out of 5 based on over <span class="text-primary"> 1000 reviews</span></h5>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 mb-4 mb-md-0">
            <div class="h-100 text-center pt-4 pt-lg-5 px-4">
              <h5 class="mb-1">Award-winning solution</h5>
              <img class="img-fluid" src="../images/svg/award.svg" alt="">
              <p>We've got a few awards thanks to our amazing clients.</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 mb-4 mb-md-0">
            <div class="h-100 bg-dark text-center p-5 p-md-4 border-radius">
              <span class="display-4 text-primary">12+</span>
              <h5 class="text-white">Clients around the globe</h5>
              <div class="owl-carousel mt-4 owl-loaded owl-drag" data-nav-arrow="false" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1" data-space="40" data-autoheight="true">
                
                
                
                
                
                
                
              <div class="owl-stage-outer owl-height" style="height: 80.75px;"><div class="owl-stage" style="transform: translate3d(-1771px, 0px, 0px); transition: all 1s ease 0s; width: 3795px;"><div class="owl-item cloned" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/04.svg" alt="">
                </div></div><div class="owl-item cloned" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/05.svg" alt="">
                </div></div><div class="owl-item cloned" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/06.svg" alt="">
                </div></div><div class="owl-item cloned" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/07.svg" alt="">
                </div></div><div class="owl-item" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/01.svg" alt="">
                </div></div><div class="owl-item" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/02.svg" alt="">
                </div></div><div class="owl-item" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/03.svg" alt="">
                </div></div><div class="owl-item active" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/04.svg" alt="">
                </div></div><div class="owl-item" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/05.svg" alt="">
                </div></div><div class="owl-item" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/06.svg" alt="">
                </div></div><div class="owl-item" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/07.svg" alt="">
                </div></div><div class="owl-item cloned" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/01.svg" alt="">
                </div></div><div class="owl-item cloned" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/02.svg" alt="">
                </div></div><div class="owl-item cloned" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/03.svg" alt="">
                </div></div><div class="owl-item cloned" style="width: 213px; margin-right: 40px;"><div class="item">
                  <img class="img-fluid center-block mx-auto" src="images/client-logo/light/04.svg" alt="">
                </div></div></div></div><div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><i class="fas fa-arrow-left"></i></button><button type="button" role="presentation" class="owl-next"><i class="fas fa-arrow-right"></i></button></div><div class="owl-dots disabled"></div></div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4">
            <div class="bg-light h-100 pt-md-0 pt-5 border-radius d-flex align-items-end ps-4 pb-4">
                <h5 class="mb-0">We're <br> epic <br> since <span class="d-block display-2 fw-bold text-stroke-primary">2018</span></h5>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="space-pt our-client">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-9">
                    <div class="section-title text-center">
                        <h2>What we do ?</h2>
                        <p>Award-winning website design &amp; creative digital agency services</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                    <div class="bg-light border border-radius h-100 overflow-hidden d-flex align-items-start flex-columnbg-light border border-radius h-100 overflow-hidden d-flex align-items-start flex-columnbg-light border border-radius h-100 overflow-hidden d-flex align-items-start flex-column">
                        <div class="p-4 mb-auto">
                            <h4>B2B Booking Engine</h4>
                            <p>Distribute your travel products across the globe<br>
                                through your sub-agent network and increase sales of business.</p>
                        </div>
                        <div class="d-flex justify-content-end w-100">
                            <img class="img-fluid w-75 me-n5" src="../images/service/01.svg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                    <div class="bg-light border border-radius h-100 overflow-hidden d-flex align-items-start flex-column">
                        <div class="p-4 mb-auto">
                            <h4>B2C Booking Engine</h4>
                            <p>B2C&nbsp;online&nbsp;booking engine&nbsp;to sell all your travel products such as hotels, flights and dynamic packages directly to customer</p>
                        </div>
                        <div class="d-flex justify-content-end w-100">
                            <img class="img-fluid w-75 me-n5" src="../images/service/02.svg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-4 mb-md-0">
                    <div class="bg-light border border-radius h-100 overflow-hidden d-flex align-items-start flex-column">
                        <div class="p-4 mb-auto">
                            <h4>XML & API Integration</h4>
                            <p>Integrate Hotel, Flight, Tour package and other travel products Xml/API from various third party suppliers and wholesalers to your travel </p>
                        </div>
                        <div class="d-flex justify-content-end w-100">
                            <img class="img-fluid w-75 me-n5" src="../images/service/03.svg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="bg-light border border-radius h-100 overflow-hidden d-flex align-items-start flex-column">
                        <div class="p-4 mb-auto">
                            <h4>Tour Operator Software</h4>
                            <p>Flexible tour operator software for travel agencies and tour operators to automate their business process and enhance customer services.</p>
                        </div>
                        <div class="d-flex justify-content-end w-100">
                            <img class="img-fluid w-75 me-n5" src="../images/service/04.svg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="space-ptb">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="feature-info feature-info-style-02 h-100">
                        <div class="feature-info-icon">
                            <i class="icofont-air-ticket"></i>
                            <h5 class="mb-0 ms-4 feature-info-title">B2B BOOKING ENGINE</h5>
                        </div>
                        <div class="feature-info-content">
                            <p class="mb-0">Do it today. Remind yourself of someone you know who died suddenly and the fact that there is no guarantee that tomorrow will come.</p>
                            <a href="B2B-Service" class="icon-btn"><i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                        <div class="feature-info-bg-img" style="background-image: url('https://lvivity.com/wp-content/uploads/2018/09/make-app.jpg');"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="feature-info feature-info-style-02 h-100">
                        <div class="feature-info-icon">
                            <i class="icofont-air-ticket"></i>
                            <h5 class="mb-0 ms-4 feature-info-title">B2C BOOKING ENGINE</h5>
                        </div>
                        <div class="feature-info-content">
                            <p class="mb-0">Positive pleasure-oriented goals are much more powerful motivators than negative fear-based ones. Although each is successful separately.</p>
                            <a href="B2C-Services" class="icon-btn"><i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                        <div class="feature-info-bg-img" style="background-image: url('https://lvivity.com/wp-content/uploads/2018/09/make-app.jpg');"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="feature-info feature-info-style-02 h-100">
                        <div class="feature-info-icon">
                            <i class="icofont-ui-flight"></i>
                            <h5 class="mb-0 ms-4 feature-info-title">FLIGHT INTEGRATION</h5>
                        </div>
                        <div class="feature-info-content">
                            <p class="mb-0">We know this in our gut, but what can we do about it? How can we motivate ourselves? One of the most difficult aspects of achieving success.</p>
                            <a href="Flight-Module" class="icon-btn"><i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                        <div class="feature-info-bg-img" style="background-image: url('https://blog.travelcarma.com/wp-content/uploads/2020/05/How-OTAs-can-integrate-Post-COVID-Airline-Policy-Changes-_-New-API-Schemas-with-TDX-header-blog-2-1024x536.png');"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="feature-info feature-info-style-02 h-100">
                        <div class="feature-info-icon">
                            <i class="icofont-pay"></i>
                            <h5 class="mb-0 ms-4 feature-info-title">Payment Gateways</h5>
                        </div>
                        <div class="feature-info-content">
                            <p class="mb-0">It must come from within as the natural product of your desire to achieve something and your belief that you are capable to succeed at your goal.</p>
                            <a href="service-detail.html" class="icon-btn"><i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                        <div class="feature-info-bg-img" style="background-image: url('https://thumbs.dreamstime.com/b/flat-design-payment-gateway-illustration-concept-graphics-come-file-types-very-easy-to-apply-any-software-192846873.jpg');"></div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="feature-info feature-info-style-02 h-100">
                        <div class="feature-info-icon">
                            <i class="icofont-bank"></i>
                            <h5 class="mb-0 ms-4 feature-info-title">Money Transfer</h5>
                        </div>
                        <div class="feature-info-content">
                            <p class="mb-0">There is really no magic to it and it’s not reserved only for a select few people. As such, success really has nothing to do with luck,</p>
                            <a href="service-detail.html" class="icon-btn"><i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                        <div class="feature-info-bg-img" style="background-image: url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTgnYA4MFZanHl3EE225CHdsijdeEVd-dmi8b5BHZrwGiOwnJ8ejoamzTl48T6o9XssSZk&usqp=CAU');"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="feature-info feature-info-style-02 h-100">
                        <div class="feature-info-icon">
                            <i class="icofont-power-zone"></i>
                            <h5 class="mb-0 ms-4 feature-info-title">Utlity Bill Payment</h5>
                        </div>
                        <div class="feature-info-content">
                            <p class="mb-0">There are basically six key areas to higher achievement. Some people will tell you there are four while others may tell you there are eight.</p>
                            <a href="service-detail.html" class="icon-btn"><i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                        <div class="feature-info-bg-img" style="background-image: url('../images/feature-info/06.jpg');"></div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
                    <div class="feature-info feature-info-style-02 h-100">
                        <div class="feature-info-icon">
                            <i class="flaticon-design"></i>
                            <h5 class="mb-0 ms-4 feature-info-title">HOTEL XML IN</h5>
                        </div>
                        <div class="feature-info-content">
                            <p class="mb-0">There is really no magic to it and it’s not reserved only for a select few people. As such, success really has nothing to do with luck,</p>
                            <a href="service-detail.html" class="icon-btn"><i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                        <div class="feature-info-bg-img" style="background-image: url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTgnYA4MFZanHl3EE225CHdsijdeEVd-dmi8b5BHZrwGiOwnJ8ejoamzTl48T6o9XssSZk&usqp=CAU');"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="feature-info feature-info-style-02 h-100">
                        <div class="feature-info-icon">
                            <i class="flaticon-group"></i>
                            <h5 class="mb-0 ms-4 feature-info-title">HOTEL XML OUT</h5>
                        </div>
                        <div class="feature-info-content">
                            <p class="mb-0">There are basically six key areas to higher achievement. Some people will tell you there are four while others may tell you there are eight.</p>
                            <a href="service-detail.html" class="icon-btn"><i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                        <div class="feature-info-bg-img" style="background-image: url('../images/feature-info/06.jpg');"></div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 d-md-flex justify-content-center align-items-center">
                    <p class="mb-3 mb-md-0 mx-0 mx-md-3">Start now! Your big opportunity may be right where you are!</p>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal" class="btn btn-primary-round btn-round mx-0 mx-md-3">Let’s Get Started<i class="fas fa-arrow-right ps-3"></i></a>
                </div>
            </div>
        </div>
    </section>


    <section class="space-pb">
      <div class="container">
        
        <div class="row g-0 pb-4 pb-md-0">
          <div class="col-lg-9">
            <div class="row">
              <div class="col-md-6 mt-5">
                <img class="img-fluid" src="../images/about/06.jpg" alt="">
              </div>
              <div class="col-md-6 mt-5">
              <div class="pe-3">
                <h3>Our work process</h3>
                  <p>The following outlines our web-development process, which can be split into several sensible elements:</p>
                  <ul class="list-unstyled list-number">
                    <li class="mb-2"><span>01</span> Technical analysis</li>
                    <li class="mb-2"><span>02</span> Planning and Idea</li>
                    <li class="mb-2"><span>03</span> Design and Copywriting</li>
                    <li class="mb-2"><span>04</span> Front-&amp; Back-End Coding</li>
                    <li class="mb-2"><span>05</span> Testing and Launch</li>
                  </ul>
                
              </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="bg-dark p-5 text-center h-100 mt-lg-0 mt-5">
              <div class="badge-round mt-2">
                <h4 class="text-white"><span class="d-block font-xlll">30+</span> Million</h4>
              </div>
              <p class="mt-3 text-white">Revenue generated by our partner and clients since 2018</p>
              <a class="btn btn-link mt-3 text-primary" href="#">View Case Study</a>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section class="space-pt bg-overlay-gradient-y-left" data-jarallax='{"speed": 0.4}' style="background-image: url(../images/slider/05.jpg);">
      <div class="container">
        <div class="row position-relative">
          <div class="col-lg-6">
            <div class="space-pb">
            <ul class="list list-unstyled">
              <li class="d-flex font-xl"><i class="flaticon-html pe-4 pt-1 font-xll text-primary"></i><span class="text-white">We make sure that the right keyword is in the page title. </span></li>
              <li class="d-flex font-xl"><i class="flaticon-app-development pe-4 pt-1 font-xll text-primary"></i><span class="text-white">We make sure that the right keyword is in the meta description. </span></li>
              <li class="d-flex font-xl"><i class="flaticon-diamond pe-4 pt-1 font-xll text-primary"></i><span class="text-white">We make sure that the right keyword is in the HTML heading tag. </span></li>
              <li class="d-flex font-xl"><i class="flaticon-icon-149196 pe-4 pt-1 font-xll text-primary"></i><span class="text-white">The keyword must be relevant to your industry. </span></li>
              <li class="d-flex font-xl"><i class="flaticon-author pe-4 pt-1 font-xll text-primary"></i><span class="text-white">It cannot be “Copied” from another website. </span></li>
              <li class="d-flex font-xl"><i class="flaticon-podcast pe-4 pt-1 font-xll text-primary"></i><span class="text-white">It must get through relevant, insightful, and informative content. Not paid content. </span></li>
              <li class="d-flex font-xl"><i class="flaticon-chart pe-4 pt-1 font-xll text-primary"></i><span class="text-white">We make sure to drives high-quality traffic to your website. </span></li>
            </ul>
            </div>
          </div>
          <div class="col-lg-5 offset-lg-1">
              <div class="bg-primary p-4 d-inline-block border-radius float-start float-lg-end mb-5">
              <span class="display-4 font-weight-blod text-white">32%</span>
              <h5 class="text-white">Average Traffic Increase</h5>
            </div>
            <div class="clearfix"> </div>
          
            <div class="section-title mt-4 mb-5 mb-lg-4">
              <h2 class="text-white">Outsourced Service In-house Feel</h2>
              <p class="text-white">Meet the outstanding performers in our industry-award-winning team of professionals.</p>
            </div>
          </div>
        </div>
      </div>
    <div id="jarallax-container-1" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden; pointer-events: none; z-index: -100;"><div style="background-position: 50% 50%; background-size: cover; background-repeat: no-repeat; background-image: url(&quot;http://localhost:64368/images/slider/05.jpg&quot;); position: fixed; top: 0px; left: 0px; width: 1263.33px; height: 460.8px; overflow: hidden; pointer-events: none; margin-top: -80.4px; transform: translate3d(0px, 147.2px, 0px);"></div></div></section>

    <br />
    <br />

    <section class="space-pb dark-background">
      <div class="container">
        <div class="bg-dark text-center rounded py-5 px-3">
          <h2 class="text-white">Tell us about your idea, and we’ll make it happen.</h2>
          <h6 class="text-white">Have a brand problem that needs to be solved? We’d love to hear about it!</h6>
          <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal" class="btn btn-primary-round btn-round mx-0 mx-md-3 text-white">Let’s Get Started<i class="fas fa-arrow-right ps-3"></i></a>
        </div>
      </div>
    </section>

    <section class="space-pb">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-9">
                    <div class="section-title text-center">
                        <h2>Smart strategy &amp; excellent performance</h2>
                        <p>Process that guarantees high productivity and profitability for your solution.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 mb-4 mb-md-0">
                    <div class="feature-info feature-info-style-05 text-center position-relative">
                        <div class="feature-info-icon">
                            <i class="flaticon-idea-1"></i>
                        </div>
                        <div class="feature-info-content">
                            <h5 class="mb-3 feature-info-title">Data Analysis </h5>
                            <p class="mb-0 px-lg-5">We conduct thorough data collection and analysis of your institution’s current strengths and differentiators.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4 mb-md-0">
                    <div class="feature-info feature-info-style-05 text-center position-relative">
                        <div class="feature-info-icon">
                            <i class="flaticon-diamond"></i>
                        </div>
                        <div class="feature-info-content">
                            <h5 class="mb-3 feature-info-title">Making a plan</h5>
                            <p class="mb-0 px-lg-5">We will work together with you to develop a plan that is actionable, feasible and tailored to your needs.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4 mb-md-0">
                    <div class="feature-info feature-info-style-05 text-center position-relative">
                        <div class="feature-info-icon">
                            <i class="flaticon-rocket"></i>
                        </div>
                        <div class="feature-info-content">
                            <h5 class="mb-3 feature-info-title">Deliver Result</h5>
                            <p class="mb-0 px-lg-5">Our role in this step is to provide expertise where needed, to augment select initiatives as desired.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-2 mt-md-5">
                <div class="col-12 d-md-flex justify-content-center align-items-center text-center">
                    <p class="mb-3 mb-md-0 mx-0 mx-md-3">Our Managed IT services will help you succeed.</p>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal" class="btn btn-primary-round btn-round mx-0 mx-md-3">Let’s Get Started<i class="fas fa-arrow-right ps-3"></i></a>
                </div>
            </div>
        </div>
    </section>

    


</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Mater.master" AutoEventWireup="true" CodeFile="our-team.aspx.cs" Inherits="Pages_our_team" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <section class="header-inner header-inner-menu bg-overlay-black-50" style="background-image: url('../images/header-inner/15.jpg');">
      <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-md-8">
            <div class="header-inner-title text-center position-relative">
              <h1 class="text-white fw-normal">Meet Our Team</h1>
              <p class="text-white mb-0">Let success motivate you. Find a picture of what epitomizes success to you and then pull it out when you are in need of motivation.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="header-inner-nav">
        <div class="container">
          <div class="row">
            <div class="col-12 d-flex justify-content-center">
              <ul class="nav">
                <li class="nav-item"><a class="nav-link" href="About_Us.aspx">About us</a></li>
                <li class="nav-item"><a class="nav-link" href="careers.aspx">Careers</a></li>
                <li class="nav-item"><a class="nav-link" href="how-we-work.aspx">How we work</a></li>
                <li class="nav-item"><a class="nav-link active" href="our-team.aspx">Our team</a></li>
                <li class="nav-item"><a class="nav-link" href="mission-vision.aspx">Mission and vision</a></li>
                <li class="nav-item"><a class="nav-link" href="our-value.aspx">Our values</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="space-ptb">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <div class="section-title">
              <h2 class="mb-3">We enable constant enterprise transformation at speed and scale.</h2>
              <p>We all carry a lot of baggage, thanks to our upbringing. The majority of people carry with them, an entire series of self-limiting beliefs that will absolutely stop, and hold them back from, success. Things like “I’m not good enough”, “I’m not smart enough”, “I’m not lucky enough”, and the worst,</p>
            </div>
            <a href="#" class="btn btn-light-round btn-round w-space">Know More About<i class="fas fa-arrow-right ps-3"></i></a>
          </div>
          <div class="col-lg-6">
            <div class="row">
              <div class="col-sm-6">
                <img class="img-fluid border-radius mb-4 mt-4" src="../images/about/05.jpg" alt="">
                <img class="img-fluid border-radius mb-4 mb-sm-0" src="../images/about/06.jpg" alt="">
              </div>
              <div class="col-sm-6">
                <img class="img-fluid border-radius mb-4" src="../images/about/07.jpg" alt="">
                <div class="counter counter-03">
                  <div class="counter-content">
                    <span class="timer" data-to="50" data-speed="20000">50</span>
                    <label>Projects Complete</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="space-pb overflow-hidden" style="display:none;">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-9">
            <div class="section-title text-center">
              <h2>Meet our heroes</h2>
              <p class="lead">Our team is friendly, talkative, and fully reliable.</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xl-2 col-md-3 col-sm-4 col-6">
            <div class="team">
              <div class="team-bg"></div>
              <div class="team-img">
                <img class="img-fluid" src="images/team/01.jpg" alt="">
              </div>
              <div class="team-info">
                <a href="#" class="team-name">Aaron Sharp</a>
                <p>Chief People Officer</p>
                <ul class="list-unstyled">
                  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-2 col-md-3 col-sm-4 col-6">
            <div class="team">
              <div class="team-bg"></div>
              <div class="team-img">
                <img class="img-fluid" src="images/team/02.jpg" alt="">
              </div>
              <div class="team-info">
                <a href="#" class="team-name">Homer Reyes</a>
                <p>Vice President</p>
                <ul class="list-unstyled">
                  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-2 col-md-3 col-sm-4 col-6">
            <div class="team">
              <div class="team-bg"></div>
              <div class="team-img">
                <img class="img-fluid" src="images/team/03.jpg" alt="">
              </div>
              <div class="team-info">
                <a href="#" class="team-name">Felica Queen</a>
                <p>Team Leader</p>
                <ul class="list-unstyled">
                  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-2 col-md-3 col-sm-4 col-6">
            <div class="team">
              <div class="team-bg"></div>
              <div class="team-img">
                <img class="img-fluid" src="images/team/04.jpg" alt="">
              </div>
              <div class="team-info">
                <a href="#" class="team-name">Sara Lisbon</a>
                <p>Production Manager</p>
                <ul class="list-unstyled">
                  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-2 col-md-3 col-sm-4 col-6">
            <div class="team">
              <div class="team-bg"></div>
              <div class="team-img">
                <img class="img-fluid" src="images/team/05.jpg" alt="">
              </div>
              <div class="team-info">
                <a href="#" class="team-name">Michael Bean</a>
                <p>Quality control</p>
                <ul class="list-unstyled">
                  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-2 col-md-3 col-sm-4 col-6">
            <div class="team">
              <div class="team-bg"></div>
              <div class="team-img">
                <img class="img-fluid" src="images/team/06.jpg" alt="">
              </div>
              <div class="team-info">
                <a href="#" class="team-name">Alice Williams </a>
                <p>Marketing manager</p>
                <ul class="list-unstyled">
                  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-2 col-md-3 col-sm-4 col-6">
            <div class="team">
              <div class="team-bg"></div>
              <div class="team-img">
                <img class="img-fluid" src="images/team/07.jpg" alt="">
              </div>
              <div class="team-info">
                <a href="#" class="team-name">Paul Flavius</a>
                <p>Human resources</p>
                <ul class="list-unstyled">
                  <li><a href="#"> <i class="fab fa-facebook-f"></i> </a></li>
                  <li><a href="#"> <i class="fab fa-twitter"></i> </a></li>
                  <li><a href="#"> <i class="fab fa-linkedin-in"></i> </a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-2 col-md-3 col-sm-4 col-6">
            <div class="team">
              <div class="team-bg"></div>
              <div class="team-img">
                <img class="img-fluid" src="images/team/08.jpg" alt="">
              </div>
              <div class="team-info">
                <a href="#" class="team-name">Anne Smith</a>
                <p>Sales and Marketing</p>
                <ul class="list-unstyled">
                  <li><a href="#"> <i class="fab fa-facebook-f"></i> </a></li>
                  <li><a href="#"> <i class="fab fa-twitter"></i> </a></li>
                  <li><a href="#"> <i class="fab fa-linkedin-in"></i> </a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-2 col-md-3 col-sm-4 col-6">
            <div class="team">
              <div class="team-bg"></div>
              <div class="team-img">
                <img class="img-fluid" src="images/team/09.jpg" alt="">
              </div>
              <div class="team-info">
                <a href="#" class="team-name">Mellissa Doe</a>
                <p>Marketing Expert</p>
                <ul class="list-unstyled">
                  <li><a href="#"> <i class="fab fa-facebook-f"></i> </a></li>
                  <li><a href="#"> <i class="fab fa-twitter"></i> </a></li>
                  <li><a href="#"> <i class="fab fa-linkedin-in"></i> </a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-2 col-md-3 col-sm-4 col-6">
            <div class="team">
              <div class="team-bg"></div>
              <div class="team-img">
                <img class="img-fluid" src="images/team/10.jpg" alt="">
              </div>
              <div class="team-info">
                <a href="#" class="team-name">Ben Aguilar</a>
                <p>Community</p>
                <ul class="list-unstyled">
                  <li><a href="#"> <i class="fab fa-facebook-f"></i> </a></li>
                  <li><a href="#"> <i class="fab fa-twitter"></i> </a></li>
                  <li><a href="#"> <i class="fab fa-linkedin-in"></i> </a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-2 col-md-3 col-sm-4 col-6">
            <div class="team">
              <div class="team-bg"></div>
              <div class="team-img">
                <img class="img-fluid" src="images/team/11.jpg" alt="">
              </div>
              <div class="team-info">
                <a href="#" class="team-name">Kim Hamilton</a>
                <p>Developer</p>
                <ul class="list-unstyled">
                  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-xl-2 col-md-3 col-sm-4 col-6">
            <div class="team apply-position">
              <div class="team-icon">
                <i class="far fa-user-circle"></i>
              </div>
              <div class="team-info">
                <a href="#" class="btn btn-link">Apply for Possition<i class="fas fa-arrow-right text-dark ps-1"></i> </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Mater.master" AutoEventWireup="true" CodeFile="Package-Module.aspx.cs" Inherits="Pages_Package_Module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .hr {
            border-top: 2px solid #e61651;
            font-weight: 700;
            width: 50px;
            margin-bottom: 10px;
        }

        p {
            text-align: justify;
        }
    </style>



    <section class="header-inner bg-overlay-black-50" style="background-image: url('../images/header-inner/17.jpg');">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="header-inner-title text-center position-relative">
                        <h1 class="text-white fw-normal">Dynamic Package Module</h1>

                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="space-ptb">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <div class="service-details">


                      
                       


                        <h5 class="fw-bold">What is Dynamic Packaging?</h5>
                        <div class="hr"></div>
                        <p>Dynamic Packaging is process of creating customized package wherein consumers can build their own package by combining multiple travel services like flights + hotels, flight + transfer, flight + tour, flight + hotel + tour, flight + car rental, etc in one package based on their choice.</p>
                        <p>Dynamic Packaging is used in package holiday bookings enabling customers to build customized package with hotels, flights, car rental and tour instead of purchasing a pre-defined package.</p>

                         <img class="img-fluid border-radius mb-4 mb-md-5" src="https://www.indochinatravel.com/country/thailand/images/thailand-header5.jpg" />


                        <h5 class="fw-bold">How Dynamic Packaging Works?</h5>
                        <div class="hr"></div>

                        <p>Dynamic Packaging works by dynamically sourcing flights, hotels, car rental and other travel components in real-time for the package. These components are then dynamically combined into packages and price is also set up dynamically. Dynamic packaging is dynamic at all levels.</p>


                        <br />
                        <br />
                        <h5 class="fw-bold">Dynamic Packaging System For Tour Operator and Travel Agent</h5>
                        <div class="hr"></div>

                        <p>Amlin Technology is leading Travel Technology Company offer best Dynamic Packaging System with powerful dynamic packaging software solution for tour operators enabling their customers to build customized package with hotels, flights, car rental and other travel services instead of purchasing a pre-defined package.</p>
                        <p>Dynamic Packaging provides a wide range of travel services including flight, hotel, transfer, and other travel services that can be reserved together as one tour or vacation package.</p>
                        <p>Dynamic Packaging System is complete Dynamic Packaging Software comes with advanced Dynamic Packaging Engine which helps to identify business rules that are applied to customer at time of booking so it becomes easy to add mark-up and dynamically package flight, hotel and other service in one vacation package.</p>
                        <p>This Dynamic Packaging System empowers tour operators and travel agents to store travel inventory of flight, hotel, tour, transfer, car rental and other travel solutions and dynamically combine it together into one tour or vacation package as per customer requirements.</p>
                        <p>Dynamic Packaging comes with advanced feature of XML API Integration which allow travel agent to get inventory of flights, hotels, etc from global travel suppliers and combine it with their inventory to build dynamic packages of multiple services such as hotels, flights, tours, transfers.</p>
                        <p>Amlin Technology have team of experienced developers who develop best Dynamic Packaging System, Dynamic Packaging Software, with advanced Dynamic Packaging Engine that come with Dynamic Packaging Solution which help travel agents or tour operator to manage inventory, define business rules, set prices and markup and build package as per customer requirements.</p>
                        <p>See how Dynamic Packaging is beneficial for Travel Business and Which are best features of Dynamic Packaging.</p>
                        <hr />


                        <h5 class="fw-bold">What are Benefits of Dynamic Packaging?</h5>
                        <div class="hr"></div>
                        <p>Dynamic Packaging Benefits allow travel companies to :</p>
                        <ul class="list list-unstyled mt-3">
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Expand their Product Offerings</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Improve Conversion Rates</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Provide travelers with one-stop shop for all Package Needs.</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Offer different travel services in one platform</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Allow package customization</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Offer competitive prices</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Simple and Easy Booking Management</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Reduces operational costs</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Increase Business Efficiency</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Increase Profits</span></li>



                        </ul>

                        <br />
                        <br />

                        <h5 class="fw-bold">Which are the best features of Dynamic Packaging?</h5>
                        <div class="hr"></div>
                        <p>See Dynamic Packaging Features :</p>
                        <ul class="list list-unstyled mt-3">
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Package and Sell multiple travel services in single platform</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Establish your own business rules</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span> Itinerary Creation</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Add Markup</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Package Choice and Customization</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span> Flexibility</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Invoice Management</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Payment system integration</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Multilanguage feature</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Multicurrency feature</span></li>
                            <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Mobile Responsive Design</span></li>
                           

                        </ul>

                        <br />
                        <p>Dynamic Packaging Module – One-stop Dynamic Packaging Software Solution to package and sell multiple travel services (flight + hotel + car rental) in one package tailor-made to suit the travelers/customer’s requirements.</p>
                     



                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>


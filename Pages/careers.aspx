﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Mater.master" AutoEventWireup="true" CodeFile="careers.aspx.cs" Inherits="Pages_careers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

    <style type="text/css">
  
        .file-upload {
            display: block;
            text-align: center;
            font-family: Helvetica, Arial, sans-serif;
            font-size: 12px;
        }

            .file-upload .file-select {
                display: block;
                border: 2px solid #f6f6f6;
                color: #34495e;
                cursor: pointer;
                height: 40px;
                line-height: 40px;
                text-align: left;
                background: #f6f6f6;
                overflow: hidden;
                position: relative;
                border-radius:5px;
            }

                .file-upload .file-select .file-select-button {
                    background: #dce4ec;
                    padding: 0 10px;
                    display: inline-block;
                    height: 40px;
                    line-height: 40px;
                }

                .file-upload .file-select .file-select-name {
                    line-height: 40px;
                    display: inline-block;
                    padding: 0 10px;
                }

                .file-upload .file-select:hover {
                    border-color: #34495e;
                    transition: all .2s ease-in-out;
                    -moz-transition: all .2s ease-in-out;
                    -webkit-transition: all .2s ease-in-out;
                    -o-transition: all .2s ease-in-out;
                }

                    .file-upload .file-select:hover .file-select-button {
                        background: #34495e;
                        color: #FFFFFF;
                        transition: all .2s ease-in-out;
                        -moz-transition: all .2s ease-in-out;
                        -webkit-transition: all .2s ease-in-out;
                        -o-transition: all .2s ease-in-out;
                    }

            .file-upload.active .file-select {
                border-color: #3fa46a;
                transition: all .2s ease-in-out;
                -moz-transition: all .2s ease-in-out;
                -webkit-transition: all .2s ease-in-out;
                -o-transition: all .2s ease-in-out;
                border-radius: 5px;
            }

                .file-upload.active .file-select .file-select-button {
                    background: #3fa46a;
                    color: #FFFFFF;
                    transition: all .2s ease-in-out;
                    -moz-transition: all .2s ease-in-out;
                    -webkit-transition: all .2s ease-in-out;
                    -o-transition: all .2s ease-in-out;
                }

            .file-upload .file-select input[type=file] {
                z-index: 100;
                cursor: pointer;
                position: absolute;
                height: 100%;
                width: 100%;
                top: 0;
                left: 0;
                opacity: 0;
                filter: alpha(opacity=0);
            }

            .file-upload .file-select.file-select-disabled {
                opacity: 0.65;
            }

                .file-upload .file-select.file-select-disabled:hover {
                    cursor: default;
                    display: block;
                    border: 2px solid #dce4ec;
                    color: #34495e;
                    cursor: pointer;
                    height: 40px;
                    line-height: 40px;
                    margin-top: 5px;
                    text-align: left;
                    background: #FFFFFF;
                    overflow: hidden;
                    position: relative;
                }

                    .file-upload .file-select.file-select-disabled:hover .file-select-button {
                        background: #dce4ec;
                        color: #666666;
                        padding: 0 10px;
                        display: inline-block;
                        height: 40px;
                        line-height: 40px;
                    }

                    .file-upload .file-select.file-select-disabled:hover .file-select-name {
                        line-height: 40px;
                        display: inline-block;
                        padding: 0 10px;
                    }

                    #loading-bar-spinner.spinner {
    left: 50%;
    margin-left: -20px;
    top: 50%;
    margin-top: -20px;
    position: absolute;
    z-index: 19 !important;
    animation: loading-bar-spinner 400ms linear infinite;
}

#loading-bar-spinner.spinner .spinner-icon {
    width: 40px;
    height: 40px;
    border:  solid 4px transparent;
    border-top-color:  #00C8B1 !important;
    border-left-color: #00C8B1 !important;
    border-radius: 50%;
}

@keyframes loading-bar-spinner {
  0%   { transform: rotate(0deg);   transform: rotate(0deg); }
  100% { transform: rotate(360deg); transform: rotate(360deg); }
}

    </style>

<%--    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
       // $(document).ready(function () {
       
      //  });
    </script>



    <section class="header-inner header-inner-menu bg-overlay-black-50" style="background-image: url('../images/header-inner/05.jpg');">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="header-inner-title text-center position-relative">
                        <h1 class="text-white fw-normal">Careers</h1>
                        <p class="text-white mb-0">Give yourself the power of responsibility. Remind yourself the only thing stopping you is yourself.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-inner-nav">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="About_Us.aspx">About us</a></li>
                            <li class="nav-item"><a class="nav-link active" href="careers.aspx">Careers</a></li>
                            <li class="nav-item"><a class="nav-link" href="how-we-work.aspx">How we work</a></li>
                            <li class="nav-item"><a class="nav-link" href="our-team.aspx">Our team</a></li>
                            <li class="nav-item"><a class="nav-link" href="mission-vision.aspx">Mission and vision</a></li>
                            <li class="nav-item"><a class="nav-link" href="our-value.aspx">Our values</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="space-ptb">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-lg-6 mb-4 mb-lg-0">
                    <div class="row g-0 d-flex align-items-end mb-4 mb-sm-2">
                        <div class="col-sm-8 pe-sm-2 mb-4 mb-sm-0">
                            <img class="img-fluid border-radius" src="../images/about/08.jpg" alt="">
                        </div>
                        <div class="col-sm-4">
                            <div class="counter counter-03 py-5">
                                <div class="counter-content">
                                    <span class="timer" data-to="491" data-speed="1000">491</span>
                                    <label>Projects Complete </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row d-flex justify-content-center">
                        <div class="col-sm-6">
                            <img class="img-fluid border-radius" src="../images/about/09.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 ps-xl-5">
                    <h2 class="mb-4">We enable constant enterprise transformation at speed and scale.</h2>
                    <p class="mb-4">Success isn’t really that difficult. There is a significant portion of the population here in North America, that actually want and need success to be hard! Why? So they then have a built-in excuse when things don’t go their way! Pretty sad situation, to say the least.</p>
                    <ul class="list list-unstyled ckeck-list">
                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Success is something of which we all want more</span></li>
                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Most people believe that success is difficult</span></li>
                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>There are basically six key areas to higher achievement</span></li>
                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Believing in yourself and those around you</span></li>
                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Making a decision to do something</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <section class="space-pb popup-gallery overflow-hidden">
        <div class="container-fluid">
            <div class="row d-flex align-items-end">
                <div class="col-md-6 col-lg-3 mb-4 mb-lg-3">
                    <a class="portfolio-img" href="../images/gallery/01.jpg">
                        <img class="img-fluid" src="images/gallery/01.jpg" alt=""></a>
                </div>
                <div class="col-md-6 col-lg-3 mb-4 mb-lg-3">
                    <a class="portfolio-img" href="../images/gallery/02.jpg">
                        <img class="img-fluid w-100" src="images/gallery/02.jpg" alt=""></a>
                </div>
                <div class="col-md-6 col-lg-3 mb-4 mb-lg-3">
                    <a class="portfolio-img" href="../images/gallery/03.jpg">
                        <img class="img-fluid" src="images/gallery/03.jpg" alt=""></a>
                </div>
                <div class="col-md-6 col-lg-3 mb-4 mb-lg-3">
                    <a class="portfolio-img" href="../images/gallery/04.jpg">
                        <img class="img-fluid" src="images/gallery/04.jpg" alt=""></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-3 mt-0 mt-lg-3">
                    <a class="portfolio-img" href="../images/gallery/05.jpg">
                        <img class="img-fluid" src="images/gallery/05.jpg" alt=""></a>
                </div>
                <div class="col-md-6 col-lg-3 mt-4 mt-lg-3">
                    <a class="portfolio-img" href="../images/gallery/06.jpg">
                        <img class="img-fluid w-100" src="images/gallery/06.jpg" alt=""></a>
                </div>
                <div class="col-md-6 col-lg-3 mt-4 mt-lg-3">
                    <a class="portfolio-img" href="../images/gallery/07.jpg">
                        <img class="img-fluid" src="images/gallery/07.jpg" alt=""></a>
                </div>
                <div class="col-md-6 col-lg-3 mt-4 mt-lg-3">
                    <a class="portfolio-img" href="../images/gallery/08.jpg">
                        <img class="img-fluid" src="images/gallery/08.jpg" alt=""></a>
                </div>
            </div>
        </div>
    </section>


    <section class="space-ptb bg-light">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-6">
                    <h2 class="mb-3 mb-lg-0">Four reasons why you should choose our service</h2>
                </div>
                <div class="col-lg-6 text-lg-end">
                    <a href="#" class="btn btn-white-round btn-round w-space">Let’s Get Started<i class="fas fa-arrow-right ps-3"></i></a>
                </div>
            </div>
            <div class="row mt-4 mt-lg-5">
                <div class="col-lg-3 col-md-6 mb-4 mb-md-5 mb-lg-0">
                    <div class="feature-info feature-info-style-06">
                        <div class="feature-info-img">
                            <img class="img-fluid" src="../images/feature-info/01.jpg" alt="">
                        </div>
                        <div class="feature-info-number mb-0">
                            <span>01</span>
                            <h5 class="mb-0 ms-4 feature-info-title">We know your business already</h5>
                        </div>
                        <p class="mt-4 mb-0">The sad thing is the majority of people have no clue about what they truly want. They have no clarity.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mb-4 mb-md-5 mb-lg-0">
                    <div class="feature-info feature-info-style-06">
                        <div class="feature-info-img">
                            <img class="img-fluid" src="../images/feature-info/02.jpg" alt="">
                        </div>
                        <div class="feature-info-number mb-0">
                            <span>02</span>
                            <h5 class="mb-0 ms-4 feature-info-title">Building context – not just code</h5>
                        </div>
                        <p class="mt-4 mb-0">What steps are required to get you to the goals? Make the plan as detailed as possible.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                    <div class="feature-info feature-info-style-06">
                        <div class="feature-info-img">
                            <img class="img-fluid" src="../images/feature-info/03.jpg" alt="">
                        </div>
                        <div class="feature-info-number mb-0">
                            <span>03</span>
                            <h5 class="mb-0 ms-4 feature-info-title">We are responsive and flexible</h5>
                        </div>
                        <p class="mt-4 mb-0">This is perhaps the single biggest obstacle that all of us must overcome in order to be successful.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="feature-info feature-info-style-06">
                        <div class="feature-info-img">
                            <img class="img-fluid" src="../images/feature-info/04.jpg" alt="">
                        </div>
                        <div class="feature-info-number mb-0">
                            <span>04</span>
                            <h5 class="mb-0 ms-4 feature-info-title">10 years experience – and counting</h5>
                        </div>
                        <p class="mt-4 mb-0">To make these virtues a habit, Franklin can up with a method to grade himself on his daily actions.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="space-ptb">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-9">
                    <div class="section-title text-center">
                        <h2>Current career opportunities at Amlin</h2>
                        <p>Positive pleasure-oriented goals are much more powerful motivators than negative fear-based ones.</p>
                    </div>
                </div>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-lg-9">
                    <div class="accordion" id="career-opportunities">
                        <div class="card">
                            <div class="accordion-icon card-header" id="headingOne">
                                <h4 class="mb-0">
                                    <button class="btn" type="button" data-bs-toggle="collapse" data-bs-target="#security-manager" aria-expanded="true" aria-controls="security-manager">IT & Security Manager</button>
                                </h4>
                            </div>
                            <div id="security-manager" class="collapse show" aria-labelledby="headingOne" data-bs-parent="#career-opportunities">
                                <div class="card-body">
                                    <p class="mb-4">From two to five he worked at his trade. The rest of the evening until 10 he spent in music, or diversion of some sort. This time is used also to put things in their places. In the last thing before retiring was examination of the day. At the age of 79, he ascribed his health to temperance; the acquisition of misfortune to industry and frugality; the confidence of his country to sincerity and justice.</p>
                                    <ul class="list list-unstyled ckeck-list mb-4 mb-md-5">
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Success is something of which we all want more</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Most people believe that success is difficult</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>There are basically six key areas to higher achievement</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Believing in yourself and those around you</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Making a decision to do something</span></li>
                                    </ul>
                                    <a href="#" class="btn btn-light-round btn-round w-space">Let’s Get Started<i class="fas fa-arrow-right ps-3"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="accordion-icon card-header" id="headingTwo">
                                <h4 class="mb-0">
                                    <button class="btn collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#business-partner" aria-expanded="false" aria-controls="business-partner">Junior HR Business Partner</button>
                                </h4>
                            </div>
                            <div id="business-partner" class="collapse" aria-labelledby="headingTwo" data-bs-parent="#career-opportunities">
                                <div class="card-body">
                                    <p class="mb-4">There are basically six key areas to higher achievement. Some people will tell you there are four while others may tell you there are eight. One thing for certain though, is that irrespective of the number of steps the experts talk about, they all originate from the same roots.</p>
                                    <ul class="list list-unstyled ckeck-list mb-4 mb-md-5">
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Making the decisio</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Clarity – developing the Vision</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Commitment – understanding the price and having the willingness to pay that price</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Belief – believing in yourself and those around you</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Taking action – practice Ready, Fire, Aim…</span></li>
                                    </ul>
                                    <a href="#" class="btn btn-light-round btn-round w-space">Let’s Get Started<i class="fas fa-arrow-right ps-3"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="accordion-icon card-header" id="headingThree">
                                <h4 class="mb-0">
                                    <button class="btn collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#outreach-representative" aria-expanded="false" aria-controls="outreach-representative">Junior Outreach Representative</button>
                                </h4>
                            </div>
                            <div id="outreach-representative" class="collapse" aria-labelledby="headingThree" data-bs-parent="#career-opportunities">
                                <div class="card-body">
                                    <p class="mb-4">Focus is having the unwavering attention to complete what you set out to do. There are a million distractions in every facet of our lives. Telephones and e-mail, clients and managers, spouses and kids, TV, newspapers and radio – the distractions are everywhere and endless. Everyone wants a piece of us and the result can be totally overwhelming.</p>
                                    <ul class="list list-unstyled ckeck-list mb-4 mb-md-5">
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>So, how can we stay on course with all the distractions</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>The best way is to develop and follow a plan. Start with your goals</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>What steps are required to get you to the goals? Make the plan as detailed as possible.</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Try to visualize and then plan for, every possible setback.</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Commit the plan to paper and then keep it with you at all times.</span></li>
                                    </ul>
                                    <a href="#" class="btn btn-light-round btn-round w-space">Let’s Get Started<i class="fas fa-arrow-right ps-3"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="accordion-icon card-header" id="headingFour">
                                <h4 class="mb-0">
                                    <button class="btn collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#salesforce-developer" aria-expanded="false" aria-controls="salesforce-developer">Senior Salesforce Developer</button>
                                </h4>
                            </div>
                            <div id="salesforce-developer" class="collapse" aria-labelledby="headingFour" data-bs-parent="#career-opportunities">
                                <div class="card-body">
                                    <p class="mb-4">Along with your plans, you should consider developing an action orientation that will keep you motivated to move forward at all times. This requires a little self-discipline, but is a crucial component to achievement of any kind. Before starting any new activity, ask yourself if that activity will move you closer to your goals.</p>
                                    <ul class="list list-unstyled ckeck-list mb-4 mb-md-5">
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>If the answer is no, you may want to reconsider doing it</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>I coach my clients to practice the 3 D’s – Defer, Delegate or Delete</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Can the particular activity be done later? Defer it!</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Commitment is something that comes from understanding that everything</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>This is important because nobody wants to put significant effort into</span></li>
                                    </ul>
                                    <a href="#" class="btn btn-light-round btn-round w-space">Let’s Get Started<i class="fas fa-arrow-right ps-3"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="accordion-icon card-header" id="headingFive">
                                <h4 class="mb-0">
                                    <button class="btn collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#development-manager" aria-expanded="false" aria-controls="development-manager">Senior Business Development Manager</button>
                                </h4>
                            </div>
                            <div id="development-manager" class="collapse" aria-labelledby="headingFive" data-bs-parent="#career-opportunities">
                                <div class="card-body pb-0">
                                    <p class="mb-4">It is truly amazing the damage that we, as parents, can inflict on our children. So why do we do it? For the most part, we don’t do it intentionally or with malice. In the majority of cases, the cause is a well-meaning but unskilled or un-thinking parent, who says the wrong thing at the wrong time, and the message sticks – as simple as that!</p>
                                    <ul class="list list-unstyled ckeck-list mb-4 mb-md-5">
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>One of the main areas that I work on with my clients</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>And it’s not just parents that are the cause – teachers</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Nothing changes until something moves</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Get the oars in the water and start rowing.</span></li>
                                        <li class="d-flex"><i class="fas fa-check pe-2 pt-1 text-primary"></i><span>Execution is the single biggest factor in achievement</span></li>
                                    </ul>
                                    <a href="#" class="btn btn-light-round btn-round w-space">Let’s Get Started<i class="fas fa-arrow-right ps-3"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4 mt-md-5">
                <div class="col text-center">
                    <p>Don’t see a role that fits? Send us your resume.</p>
                    <a href="#" class="btn btn-primary btn-round text-white w-space" data-bs-toggle="modal" data-bs-target="#resume">Submit Your Resume<i class="fas fa-arrow-right ps-3"></i></a>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="resume" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Upload your resume</h5>

                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <div class="">
                        <div class="mb-3">
                            <asp:TextBox ID="txt_fullname" runat="server" placeholder="Full Name"  class="form-control"></asp:TextBox>
<%--                            <input type="text" class="form-control" id="exampleInputName" placeholder="Full Name">--%>
                        </div>
                        <div class="mb-3">
                               <asp:TextBox ID="txt_mob_no" runat="server" placeholder="Mobile No."  class="form-control"></asp:TextBox>

<%--                            <input type="text" class="form-control" id="exampleInputMobile" placeholder="Mobile No.">--%>
                        </div>
                        <div class="mb-3">
                                 <asp:TextBox ID="txt_email1" runat="server" placeholder="Email Address"  class="form-control"></asp:TextBox>

<%--                            <input type="text" class="form-control" id="exampleInputEmail" placeholder="Email Address">--%>
                        </div>
                      

                        <div class="file-upload">
                            <div class="file-select">
                                <div class="file-select-button" id="fileName">Upload Resume</div>
                                <div class="file-select-name" id="noFile">No file chosen...</div>
<%--                                <input type="file" name="chooseFile" class="form-control" id="chooseFile">--%>
                                <asp:FileUpload ID="chooseFile" class="form-control"  runat="server" />
                            </div>
                        </div>

                   <%--     <div class="mb-4">
                            <div class="form-check ms-1">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                    I agree to talk about my project with Amlin Technology
                                </label>
                            </div>
                        </div>--%>
                        <br />
                        <br />
                        <div class="mb-0">
                            <asp:Button ID="Button1" class="btn btn-primary"  runat="server" Text="Send Massage" OnClick="Button1_Click" OnClientClick="return ValidateForm()"  /><asp:Label ID="lbs" runat="server" Text=""></asp:Label>
                            <div id="loading-bar-spinner" class="spinner" style="display:none;"><div class="spinner-icon"></div></div>
<%--                            <button type="submit" class="btn btn-primary">Send Massage<i class="fas fa-arrow-right ps-3"></i></button>--%>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>

        <script type="text/javascript">

            function ValidateForm() {

                if ($("[id$=txt_fullname]").val() == "") {
                   // alert("Please Enter Description")
                    $("[id$=txt_fullname]").attr('placeholder',
                        'Please Enter Full Name');
                    $("[id$=txt_fullname]").focus().css('border-color','#ef3139');
                    return false;
                }
                if ($("[id$=txt_mob_no]").val() == "")
                {
                    $("[id$=txt_mob_no]").attr('placeholder',
                        'Please Enter Mobile No.');
                    $("[id$=txt_mob_no]").focus();
                    return false;
                }
                if ($("[id$=txt_email1]").val() == "") {
                    $("[id$=txt_email1]").attr('placeholder',
                        'Please Enter Email ID');
                    $("[id$=txt_email1]").focus();
                    return false;
                }
                if ($("[id$=chooseFile]").val() == "") {
                       $("#noFile").text("Please choose File...");

                    $("[id$=chooseFile]").focus();
                    return false;
                }
                if ($("[id$=chooseFile]").val() != '')
                {
                    var ext = $("[id$=chooseFile]").val().split(".").pop().toLowerCase();
                    if ($.inArray(ext, ["doc", "pdf", 'docx']) == -1) {
                        $("#noFile").text("Please choose file PDF Or DOCX format only");

                        $("[id$=chooseFile]").focus();
                        return false;
                    } else {
                    }
                }
                else {
                   
                    $("#loading-bar-spinner").css("display", "block");
                    $("#loading-bar-spinner").show().delay(10000).hide(0);
                    return true;
                }

            }



            $("[id$=chooseFile]").bind('change', function () {
                var filename = $("[id$=chooseFile]").val();
                if (/^\s*$/.test(filename)) {

                    $(".file-upload").removeClass('active');
                    $("#noFile").text("No file chosen...");
                }
                else {

                    $(".file-upload").addClass('active');
                    $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
                }
            });



        </script>
</asp:Content>


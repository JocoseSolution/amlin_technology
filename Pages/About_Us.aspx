﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Mater.master" AutoEventWireup="true" CodeFile="About_Us.aspx.cs" Inherits="Pages_About_Us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <section class="header-inner header-inner-menu bg-overlay-black-50" style="background-image: url('../images/header-inner/01.jpg');">
      <div class="container position-relative">
        <div class="row d-flex justify-content-center position-relative">
          <div class="col-md-8">
            <div class="header-inner-title text-center">
              <h1 class="text-white fw-normal">About Amlin</h1>
              <p class="text-white mb-0">Our Expertise. Know more about what we do</p>
            </div>
          </div>
        </div>
      </div>
      <div class="header-inner-nav">
        <div class="container">
          <div class="row">
            <div class="col-12 d-flex justify-content-center">
              <ul class="nav">
                <li class="nav-item"><a class="nav-link active" href="About_Us.aspx">About us</a></li>
                <li class="nav-item"><a class="nav-link" href="careers.aspx">Careers</a></li>
                <li class="nav-item"><a class="nav-link" href="how-we-work.aspx">How we work</a></li>
                <li class="nav-item"><a class="nav-link" href="our-team.aspx">Our team</a></li>
                <li class="nav-item"><a class="nav-link" href="mission-vision.aspx">Mission and vision</a></li>
                <li class="nav-item"><a class="nav-link" href="our-value.aspx">Our values</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section class="space-ptb">
      <div class="container">
        <div class="row d-flex align-items-center">
          <div class="col-lg-6 mb-4 mb-lg-0">
            <div class="row g-0 d-flex align-items-end mb-4 mb-sm-2">
              <div class="col-sm-8 pe-sm-2 mb-4 mb-sm-0">
                <img class="img-fluid border-radius" src="../images/about/08.jpg" alt="">
              </div>
         <%--     <div class="col-sm-4">
                <div class="counter counter-03 py-5">
                  <div class="counter-content">
                    <span class="timer" data-to="50" data-speed="1000">50</span>
                    <label>Projects Complete </label>
                  </div>
                </div>
              </div>--%>
            </div>
            <div class="row d-flex justify-content-center">
              <div class="col-sm-6">
                <img class="img-fluid border-radius" src="../images/about/09.jpg" alt="">
              </div>
            </div>
          </div>
          <div class="col-lg-6 ps-xl-5">
            <p class="mb-4">In today’s technological era – regardless of the nature of the business or industry -it is imperative for company to have a strong foothold on the web in order to boost its digital presence and elevate the activities to new heights. This is where the techniques of manipulating your website come in.</p>
            <p class="mb-4">We, at Techno Heaven, have a team of streamlined professionals dedicated to the perfection and uniqueness in providing a whole spectrum of web related services, such as Website Designing, Software Development, E-commerce Solutions, and Email Marketing, along with Pay per Click and Search Engine Optimization, to ensure the highest ROI out of your website.</p>
     <p class="mb-4">Contact our specialists with hands-on experience to get advice on IT solutions and enhance your business growth.</p>
          </div>
        </div>
      </div>
    </section>


    <section class="py-4 bg-transparant border">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-sm-6">
            <div class="counter counter-02">
              <div class="counter-icon align-self-center">
                <i class="flaticon-emoji"></i>
              </div>
              <div class="counter-content align-self-center">
                <span class="timer" data-to="15" data-speed="10000">15</span>
                <label>Happy Clients</label>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-sm-6">
            <div class="counter counter-02">
              <div class="counter-icon">
                <i class="flaticon-trophy"></i>
              </div>
              <div class="counter-content">
                <span class="timer" data-to="20" data-speed="20000">20</span>
                <label>Skilled Experts</label>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-sm-6">
            <div class="counter counter-02">
              <div class="counter-icon">
                <i class="flaticon-strong"></i>
              </div>
              <div class="counter-content">
                <span class="timer" data-to="50" data-speed="20000">50</span>
                <label>Finished Projects</label>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-sm-6">
            <div class="counter counter-02">
              <div class="counter-icon">
                <i class="flaticon-icon-149196"></i>
              </div>
              <div class="counter-content">
                <span class="timer" data-to="1090" data-speed="20000">1090</span>
                <label>Media Posts</label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

</asp:Content>


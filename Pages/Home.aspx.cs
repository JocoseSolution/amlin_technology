﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string SendEmailFromPortal(string firstname, string lastname, string email, string type, string desc)
    {
        string rutStr = string.Empty;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString.ToString());
        try
        {
            con.Open();
            SqlCommand oCommand = new SqlCommand("InsertEnquiry", con);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.Add("@name", SqlDbType.VarChar).Value = firstname;
            oCommand.Parameters.Add("@last_name", SqlDbType.VarChar).Value = lastname;
            oCommand.Parameters.Add("@email", SqlDbType.VarChar).Value = email;
            oCommand.Parameters.Add("@enquiry_type", SqlDbType.VarChar).Value = type;
            oCommand.Parameters.Add("@enquiry_des", SqlDbType.VarChar).Value = desc;
            oCommand.ExecuteNonQuery();
            con.Close();
            string fullname = firstname + " " + lastname;

            UtilityService.SendApplicantEmail(fullname, email);
            if (UtilityService.SendToAdminEmail(fullname, email, type, desc) > 0)
            {
                rutStr = "success";
            }
            else
            {
                rutStr = "fail";
            }
        }
        catch (Exception ex)
        {
            con.Close();
            rutStr = "fail";
        }
        return rutStr;
    }
}
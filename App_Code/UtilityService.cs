﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public static class UtilityService
{
    public static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString.ToString());
    public static void SendApplicantEmail(string fullname, string ToEmail)
    {
        string mailto = "";
        String HtmlBody = "";
        string mailfrom = "";
        string smtp = "";
        string userid = "";
        string pass = "";
        string SubmitDate = "";

        try
        {

            SqlCommand cmd = new SqlCommand("SELECT * FROM EmaiL_Temp WHERE USER_Name='ToEnquiry'", con);
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                HtmlBody = dr["Html_Body"].ToString();
            }
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }


        try
        {
            SqlCommand cmd2 = new SqlCommand("SELECT * FROM Email_Crendentials WHERE username='admin'", con);
            cmd2.CommandType = CommandType.Text;
            //SqlCommand cmd2 = new SqlCommand("SELECT * FROM Email_Crendentials where username='" + user1 + "'", con);

            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            SqlDataReader dr2 = cmd2.ExecuteReader();
            while (dr2.Read())
            {

                mailfrom = dr2["mailfrom"].ToString();
                smtp = dr2["smtpclient"].ToString();
                userid = dr2["userid"].ToString();
                pass = dr2["password"].ToString();
                mailto = dr2["email"].ToString();
                HtmlBody = dr2["Html_Body"].ToString();

            }
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }
        string subject = "Thank You for contacting us";
        DateTime now = DateTime.Now;
        SubmitDate = now.ToString("dd/MM/yyyy");
        HtmlBody = HtmlBody.Replace("#guest", fullname);
        login ls = new login();
        int i = ls.SendMail(ToEmail, mailfrom, smtp, userid, pass, HtmlBody, subject, "");
        if (i > 0)
        {
            //lbs.Text = "Submitted Successfuly";
            // Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LoggedInScript", "alert('Thank you for submitting, we will contact you very soon')", true);
            /// lbs.Visible = false;
        }
        else
        {
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LoggedInScript", "alert('Wotk Assign Successfully, Mail not sent to develpoer ')", true);
            // lbs.Visible = false;

        }
    }
    public static int SendToAdminEmail(string fullname, string email, string etype, String dis)
    {
        String HtmlBody = "";
        string mailto = "";
        string mailfrom = "";
        string smtp = "";
        string userid = "";
        string pass = "";
        string SubmitDate = "";
        try
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM EmaiL_Temp WHERE USER_Name='NewEnqToAdmin'", con);

            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                HtmlBody = dr["Html_Body"].ToString();


            }
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }
        try
        {
            SqlCommand cmd2 = new SqlCommand("SELECT * FROM Email_Crendentials WHERE username='admin'", con);
            cmd2.CommandType = CommandType.Text;
            //SqlCommand cmd2 = new SqlCommand("SELECT * FROM Email_Crendentials where username='" + user1 + "'", con);

            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            SqlDataReader dr2 = cmd2.ExecuteReader();
            while (dr2.Read())
            {

                mailfrom = dr2["mailfrom"].ToString();
                smtp = dr2["smtpclient"].ToString();
                userid = dr2["userid"].ToString();
                pass = dr2["password"].ToString();
                mailto = dr2["email"].ToString();
                HtmlBody = dr2["Html_Body"].ToString();
            }
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }




        string subject = "New Enquiry from" + " " + fullname;
        DateTime now = DateTime.Now;
        SubmitDate = now.ToString("dd/MM/yyyy");
        HtmlBody = HtmlBody.Replace("#Name", fullname).Replace("#email", email).Replace("#etype", etype).Replace("#des", dis);
        login ls = new login();
        return SendMail1(mailfrom, mailfrom, smtp, userid, pass, HtmlBody, subject, "");
      
    }
    public static int SendMail1(string toEMail, string from, string smtpClient, string userID, string pass, string body, string subject, string path)
    {
        System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
        System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
        msgMail.To.Clear();
        msgMail.To.Add(new System.Net.Mail.MailAddress(toEMail));
        msgMail.From = new System.Net.Mail.MailAddress(from);


        msgMail.Subject = subject;
        msgMail.IsBodyHtml = true;
        msgMail.Body = body;


        try
        {
            objMail.Credentials = new System.Net.NetworkCredential(userID, pass);
            objMail.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            objMail.EnableSsl = false;

            objMail.Port = 25;
            objMail.Host = smtpClient;
            // objMail.Timeout = 10000
            objMail.Send(msgMail);
            return 1;
        }
        catch (Exception ex)
        {
            // clsErrorLog.LogInfo(ex);
            return 0;
        }
    }
}
﻿using DatabaseConnect;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;

public class login
{

    ConnectionManager con = new ConnectionManager();
    DataSet dss = new DataSet();



    public DataSet Check_status(string USERID, string USERPASS)
    {
        DataSet ViewCom = new DataSet();
        string SqlQuery = "SELECT * FROM eimas_cr WHERE  eimas_cr.EIMAS_CR_NO='" + USERID.ToString() + "' AND eimas_cr.EIMAS_CR_PASSWORD='" + USERPASS.ToString() + "'";
        ViewCom = new ConnectionManager().data_set(SqlQuery);
        return ViewCom;


    }

    public DataSet Check_Old_Password(string USERID, string USERPASS)
    {
        DataSet ViewCom = new DataSet();
        string SqlQuery = "SELECT * FROM eimas_cr WHERE  eimas_cr.EIMAS_CR_NO='" + USERID.ToString() + "' AND eimas_cr.EIMAS_CR_PASSWORD='" + USERPASS.ToString() + "'";
        ViewCom = new ConnectionManager().data_set(SqlQuery);
        return ViewCom;


    }
    public DataSet GetFullUserName(string USERID)
    {
        DataSet ViewCom = new DataSet();
        string SqlQuery = "SELECT * FROM eimas_cr WHERE  eimas_cr.EIMAS_CR_NO='" + USERID.ToString() + "'";
        ViewCom = new ConnectionManager().data_set(SqlQuery);
        return ViewCom;


    }
    public void up(string date, string des, string st, string id, string priority, string work)
    {

        SqlConnection con = ConnectionManager.con;
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@DATE", date);
        param[1] = new SqlParameter("@DES", des);
        param[2] = new SqlParameter("@status", st);
        param[3] = new SqlParameter("@ID", id);
        param[4] = new SqlParameter("@priority", priority);
        param[5] = new SqlParameter("@work", work);


        SqlHelper.ExecuteNonQuery(con, "sp_udate", param);


    }

    public DataSet open_close_in(String flag)
    {
        SqlConnection con = ConnectionManager.con;
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@flag", flag);
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, "open_cl_process", param);
        return ds;

    }

    //public DataTable UserInfo(String userid)
    //{
    //    SqlConnection con = ConnectionManager.con;
    //    SqlParameter[] param = new SqlParameter[1];
    //    param[0] = new SqlParameter("@userid", userid);
    //    DataTable ds = new DataTable();
    //    ds = SqlHelper.ExecuteDataset(con, "UserInfo", param);
    //    return ds;

    //}

    //public DataSet searching(string flag, string frmdate, string todate)
    //{
    //    SqlConnection con = ConnectionManager.con;
    //    SqlParameter[] param = new SqlParameter[3];
    //    param[0] = new SqlParameter("@flag", flag);
    //    param[1] = new SqlParameter("@frmdate", frmdate);
    //    param[2] = new SqlParameter("@todate", todate);
    //    DataSet ds = new DataSet();
    //    ds = SqlHelper.ExecuteDataset(con, "search_sp", param);
    //    return ds;


    //}
    public DataSet searching1(string flag, string frmdate, string todate, string EIMAS_CR_TCKT_CRN_NO)
    {

        SqlCommand cmd = new SqlCommand("search_sp", ConnectionManager.con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@flag", flag);
        cmd.Parameters.AddWithValue("@frmdate", frmdate);
        cmd.Parameters.AddWithValue("@todate", todate);
        cmd.Parameters.AddWithValue("@EIMAS_CR_TCKT_CRN_NO", EIMAS_CR_TCKT_CRN_NO);
        SqlDataAdapter ad = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        ad.Fill(ds, "Det");
        return ds;


    }

    public DataSet SerachByPriority(string flag, string prio, string EIMAS_CR_TCKT_CRN_NO)
    {

        SqlCommand cmd = new SqlCommand("SearchByPriority", ConnectionManager.con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@flag", flag);
        cmd.Parameters.AddWithValue("@prio", prio);
        cmd.Parameters.AddWithValue("@userid", EIMAS_CR_TCKT_CRN_NO);
        SqlDataAdapter ad = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        ad.Fill(ds, "Det");
        return ds;


    }

    public DataSet searching2(string flag, string frmdate, string todate,string priority,string agency,string dev)
    {

        SqlCommand cmd = new SqlCommand("search_sp", ConnectionManager.con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@flag", flag);
        cmd.Parameters.AddWithValue("@frmdate", frmdate);
        cmd.Parameters.AddWithValue("@todate", todate);
        cmd.Parameters.AddWithValue("@Priority", priority);
        cmd.Parameters.AddWithValue("@agency", agency);
        cmd.Parameters.AddWithValue("@dev", dev);
        cmd.Parameters.AddWithValue("@EIMAS_CR_TCKT_CRN_NO", "");
        SqlDataAdapter ad = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        ad.Fill(ds, "Det");
        return ds;


    }



    public int SendMail(string toEMail, string from, string smtpClient, string userID, string pass, string body, string subject, string AttachmentFile)
    {
        System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
        System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
        msgMail.To.Clear();
        try
        {
            msgMail.To.Add(new System.Net.Mail.MailAddress(toEMail));
            msgMail.From = new System.Net.Mail.MailAddress(from);
        if (AttachmentFile != "")
        {
            msgMail.Attachments.Add(new System.Net.Mail.Attachment(AttachmentFile));
        }
        msgMail.Subject = subject;
            msgMail.IsBodyHtml = true;
            msgMail.Body = body;



            objMail.Credentials = new System.Net.NetworkCredential(userID, pass);
            objMail.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            objMail.EnableSsl = true;
        
            objMail.Port = 587;
            objMail.Host = smtpClient;
            // objMail.Timeout = 10000
            objMail.Send(msgMail);
            return 1;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }



    //public int SendMail1(string toEMail, string from, string bcc, string cc, string smtpClient, string userID, string pass, string body, string subject, string AttachmentFile)
    //{
    //    System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
    //    System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
    //    msgMail.To.Clear();
    //    msgMail.To.Add(new System.Net.Mail.MailAddress(toEMail));
    //    msgMail.From = new System.Net.Mail.MailAddress(from);
       
    //    if (AttachmentFile != "")
    //        msgMail.Attachments.Add(new System.Net.Mail.Attachment(AttachmentFile));

    //    msgMail.Subject = subject;
    //    msgMail.IsBodyHtml = true;
    //    msgMail.Body = body;


    //    try
    //    {
    //        objMail.Credentials = new System.Net.NetworkCredential(userID, pass);
    //        objMail.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
    //        objMail.EnableSsl = false;

    //        objMail.Port = 25;
    //        objMail.Host = smtpClient;
    //        // objMail.Timeout = 10000
    //        objMail.Send(msgMail);
    //        return 1;
    //    }
    //    catch (Exception ex)
    //    {
    //       // clsErrorLog.LogInfo(ex);
    //        return 0;
    //    }
    //}

    public int SendMail(string mailfrom1, string mailfrom2, string smtp, string userid, string pass, string htmlBody, string subject, Attachment data)
    {
        throw new NotImplementedException();
    }
}

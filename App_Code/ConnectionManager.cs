﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace DatabaseConnect
{
    public class ConnectionManager
    {

        public static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString);
        public static SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString);
        public ConnectionManager()
        {


        }

        public static SqlCommand sql_command(string storedproc)
        {

            SqlCommand cmd = new SqlCommand(storedproc, con);
            return cmd;
        }

        public static void NonQuery(string queryString)
        {

            if (ConnectionManager.con.State == ConnectionState.Open)
            {
                ConnectionManager.con.Close();
            }
            try
            {
                SqlCommand command = new SqlCommand(queryString, con);
                con.Open();
                command.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                ConnectionManager.con.Close();

            }

        }
        public static SqlDataReader ReaderQuery(string querystr)
        {
            if (ConnectionManager.con.State == ConnectionState.Open)
            {
                ConnectionManager.con.Close();
            }

            if (ConnectionManager.con1.State == ConnectionState.Open)
            {
                ConnectionManager.con1.Close();
            }

            try
            {
                SqlDataReader rd;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = querystr;
                con1.Open();
                cmd.Connection = con1;
                rd = cmd.ExecuteReader();
                return rd;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public DataSet data_set(string squery)
        {
            DataSet dsss = new DataSet();
            SqlConnection connection = new SqlConnection()
            {
                ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ConnectionString
            };
            SqlCommand command = new SqlCommand()
            {
                CommandText = squery,
                CommandType = CommandType.Text,
                Connection = connection
            };
            SqlDataAdapter adapter = new SqlDataAdapter()
            {
                SelectCommand = command
            };
            adapter.Fill(dsss);
            adapter.Dispose();
            return dsss;
        }



    }

}



﻿CallByDuty = () => {
    $(".enqform").css("display", "block");
    $("#enqsent").css("display", "none");
    $("#hmessage").html();
    $("#txt_name").css('border-color', '#eeeeee').val("");
    $("#txt_last_name").css('border-color', '#eeeeee').val("");
    $("#txt_email").css('border-color', '#eeeeee').val("");
    $("#txt_enquiry_type").css('border-color', '#eeeeee').val("");
    $("#txt_des").css('border-color', '#eeeeee').val("");
    $("#btnEnqSend").html("Send Massage<i class='fas fa-arrow-right ps-3'></i>");
}
SendEnqDetails = () => {
    if (ValidateForm1()) {
        let txt_name = $("#txt_name").val();
        let txt_last_name = $("#txt_last_name").val();
        let txt_email = $("#txt_email").val();
        let txt_enquiry_type = $("#txt_enquiry_type").val();
        let txt_des = $("#txt_des").val();

        $("#btnEnqSend").html("Sending...<i class='fa fa-spinner fa-pulse'></i>");

        $.ajax({
            type: "Post",
            url: "/Pages/Home.aspx/SendEmailFromPortal",
            data: '{firstname: ' + JSON.stringify(txt_name) + ',lastname: ' + JSON.stringify(txt_last_name) + ',email: ' + JSON.stringify(txt_email) + ',type: ' + JSON.stringify(txt_enquiry_type) + ',desc: ' + JSON.stringify(txt_des) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                debugger;
                if (response.d != null && response.d != "") {
                    if (response.d == "success") {
                        $(".enqform").css("display", "none");
                        $("#enqsent").css("display", "block");
                        $("#hmessage").html("Thanks for contacting us! We will be in touch with you shortly.").css("color","green");
                    }
                    else {
                        $(".enqform").css("display", "none");
                        $("#enqsent").css("display", "block");
                        $("#hmessage").html("An error occurred while sending mail.").css("color", "red");
                    }
                }
            }
        });
    }
}

function ValidateForm1() {

    if ($("#txt_name").val() == "") {
        $("#txt_name").focus().css('border-color', '#ef3139');
        return false;
    }
    if ($("#txt_last_name").val() == "") {
        $("#txt_last_name").focus().css('border-color', '#ef3139');
        return false;
    }
    if ($("#txt_email").val() == "") {
        $("#txt_email").focus().css('border-color', '#ef3139');
        return false;
    }
    if ($('#flexCheckDefault:checked').length == 0) {
        $("#flexCheckDefault").focus();
        return false;
    }
    return true;
}